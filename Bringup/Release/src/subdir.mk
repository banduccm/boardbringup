################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
..\src/Bringup.c \
..\src/adc.c \
..\src/conserver.c \
..\src/dbsct.c \
..\src/gpio.c \
..\src/heater.c \
..\src/i2c.c \
..\src/pump.c \
..\src/reset_program.c \
..\src/sbrk.c \
..\src/spi.c \
..\src/subpump.c 

OBJS += \
./src/Bringup.obj \
./src/adc.obj \
./src/conserver.obj \
./src/dbsct.obj \
./src/gpio.obj \
./src/heater.obj \
./src/i2c.obj \
./src/pump.obj \
./src/reset_program.obj \
./src/sbrk.obj \
./src/spi.obj \
./src/subpump.obj 

C_DEPS += \
./src/Bringup.d \
./src/adc.d \
./src/conserver.d \
./src/dbsct.d \
./src/gpio.d \
./src/heater.d \
./src/i2c.d \
./src/pump.d \
./src/reset_program.d \
./src/sbrk.d \
./src/spi.d \
./src/subpump.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.obj: ../src/%.c
	@echo 'Scanning and building file: $<'
	@echo 'Invoking: Scanner and Compiler'
	scandep1 -MM -MP -MF"$(@:%.obj=%.d)" -MT"$(@:%.obj=%.obj)" -MT"$(@:%.obj=%.d)"   -I"C:\PROGRA~2\Renesas\RX\2_3_0/include" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\inc" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2\i_src" -D__RX   -D__RXV1=1 -D__DBL8=1 -D__FPU=1  -D__LIT=1 -D__RON=1 -D__UCHAR=1 -D__UBIT=1 -D__BITRIGHT=1 -D__DOFF=1 -D__INTRINSIC_LIB=1 -D_STDC_VERSION_=199409L -U_STDC_HOSTED_  -D__RENESAS__=1 -D__RENESAS_VERSION__=0x02030000 -D__RX=1 -U_WIN32 -UWIN32 -U__WIN32__ -U__GNUC__ -U__GNUC_MINOR__ -U__GNUC_PATCHLEVEL__   -E -quiet -I. -C "$<"
	ccrx -lang=c -output=obj="$(@:%.d=%.obj)"  -include="C:\PROGRA~2\Renesas\RX\2_3_0/include","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\inc","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2\i_src"  -isa=rxv1 -dbl_size=8 -fpu -nologo  -define=__RX   "$<"
	@echo 'Finished scanning and building: $<'
	@echo.

