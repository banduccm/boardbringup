################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
..\src/PDG2/IO/R_PG_IO_PORT.c \
..\src/PDG2/IO/R_PG_IO_PORT_P0.c \
..\src/PDG2/IO/R_PG_IO_PORT_P1.c \
..\src/PDG2/IO/R_PG_IO_PORT_P2.c \
..\src/PDG2/IO/R_PG_IO_PORT_P4.c \
..\src/PDG2/IO/R_PG_IO_PORT_P5.c \
..\src/PDG2/IO/R_PG_IO_PORT_P6.c \
..\src/PDG2/IO/R_PG_IO_PORT_P8.c \
..\src/PDG2/IO/R_PG_IO_PORT_PA.c \
..\src/PDG2/IO/R_PG_IO_PORT_PB.c \
..\src/PDG2/IO/R_PG_IO_PORT_PC.c \
..\src/PDG2/IO/R_PG_IO_PORT_PD.c \
..\src/PDG2/IO/R_PG_IO_PORT_PE.c 

OBJS += \
./src/PDG2/IO/R_PG_IO_PORT.obj \
./src/PDG2/IO/R_PG_IO_PORT_P0.obj \
./src/PDG2/IO/R_PG_IO_PORT_P1.obj \
./src/PDG2/IO/R_PG_IO_PORT_P2.obj \
./src/PDG2/IO/R_PG_IO_PORT_P4.obj \
./src/PDG2/IO/R_PG_IO_PORT_P5.obj \
./src/PDG2/IO/R_PG_IO_PORT_P6.obj \
./src/PDG2/IO/R_PG_IO_PORT_P8.obj \
./src/PDG2/IO/R_PG_IO_PORT_PA.obj \
./src/PDG2/IO/R_PG_IO_PORT_PB.obj \
./src/PDG2/IO/R_PG_IO_PORT_PC.obj \
./src/PDG2/IO/R_PG_IO_PORT_PD.obj \
./src/PDG2/IO/R_PG_IO_PORT_PE.obj 

C_DEPS += \
./src/PDG2/IO/R_PG_IO_PORT.d \
./src/PDG2/IO/R_PG_IO_PORT_P0.d \
./src/PDG2/IO/R_PG_IO_PORT_P1.d \
./src/PDG2/IO/R_PG_IO_PORT_P2.d \
./src/PDG2/IO/R_PG_IO_PORT_P4.d \
./src/PDG2/IO/R_PG_IO_PORT_P5.d \
./src/PDG2/IO/R_PG_IO_PORT_P6.d \
./src/PDG2/IO/R_PG_IO_PORT_P8.d \
./src/PDG2/IO/R_PG_IO_PORT_PA.d \
./src/PDG2/IO/R_PG_IO_PORT_PB.d \
./src/PDG2/IO/R_PG_IO_PORT_PC.d \
./src/PDG2/IO/R_PG_IO_PORT_PD.d \
./src/PDG2/IO/R_PG_IO_PORT_PE.d 


# Each subdirectory must supply rules for building sources it contributes
src/PDG2/IO/%.obj: ../src/PDG2/IO/%.c
	@echo 'Scanning and building file: $<'
	@echo 'Invoking: Scanner and Compiler'
	scandep1 -MM -MP -MF"$(@:%.obj=%.d)" -MT"$(@:%.obj=%.obj)" -MT"$(@:%.obj=%.d)"   -I"C:\PROGRA~2\Renesas\RX\2_3_0/include" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\inc" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2" -I"C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2\i_src" -D__RX   -D__RXV1=1 -D__DBL8=1 -D__FPU=1  -D__LIT=1 -D__RON=1 -D__UCHAR=1 -D__UBIT=1 -D__BITRIGHT=1 -D__DOFF=1 -D__INTRINSIC_LIB=1 -D_STDC_VERSION_=199409L -U_STDC_HOSTED_  -D__RENESAS__=1 -D__RENESAS_VERSION__=0x02030000 -D__RX=1 -U_WIN32 -UWIN32 -U__WIN32__ -U__GNUC__ -U__GNUC_MINOR__ -U__GNUC_PATCHLEVEL__   -E -quiet -I. -C "$<"
	ccrx -lang=c -output=obj="$(@:%.d=%.obj)"  -include="C:\PROGRA~2\Renesas\RX\2_3_0/include","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\inc","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2","C:\Workspace\e2studio\FortPittGroup\ControlBoard\Bringup\src\PDG2\i_src"  -isa=rxv1 -dbl_size=8 -fpu -nologo  -define=__RX   "$<"
	@echo 'Finished scanning and building: $<'
	@echo.

