/*
 * pump.h
 *
 *  Created on: May 6, 2015
 *      Author: Mike
 */

#ifndef PUMP_H_
#define PUMP_H_

#include "gpio.h"
#include "adc.h"

typedef struct pump_s
{
    gpio_t  enable;
    gpio_t  overcurrent;
    adc_t   current_mA;
} pump_t;

bool pumpInit(void);
bool pumpEnable(pump_t*, bool);

#endif /* PUMP_H_ */
