/*
 * testIntercept.h
 *
 *  Created on: Aug 3, 2015
 *      Author: Mike
 */

#ifndef TESTINTERCEPT_H_
#define TESTINTERCEPT_H_

//Functions to be intercepted when testing - mostly hardware / RPDL calls

#ifdef UNIT_TEST
    #define gpioWrite(x, y) gpioMockWrite(x, y)
    #define gpioRead(x, y) gpioMockRead(x, y)
#endif

#endif /* TESTINTERCEPT_H_ */
