/*
 * testContainer.h
 *
 *  Created on: Aug 3, 2015
 *      Author: Mike
 */

#ifndef TESTCONTAINER_H_
#define TESTCONTAINER_H_

#include "NextGen_Quad_Conserver_Brd.h"
#include "conserver.h"
#include "controlTasks.h"
#include "heaterManager.h"

//Would like to have something trip a breakpoint or... something? __LINE__ and __FILE__ don't exist in CCRX I guess?
//Thanks to MinUnit for showing me what this code *should* look like -_- (http://www.jera.com/techinfo/jtns/jtn002.html)
#define assert(message, test) do { if (!(test)) return; } while (0)

void testHeaterController(void);
void disableAll(heaterPair_t*);
void checkHeaterEnableStatus(heaterPair_t*, bool);


#endif /* TESTCONTAINER_H_ */
