#ifndef HEATER_H_
#define HEATER_H_

#include "gpio.h"
#include "adc.h"

typedef struct heater_s
{
    //GPIOs
    gpio_t  enable;
    gpio_t  overcurrent;

    //ADC
    adc_t   current_mA;

    //Status
    bool    enableStatus;
} heater_t;

typedef struct heaterPair_s
{
    //Heaters
    heater_t    mainHeater;
    heater_t    wrapHeater;

    //Status
    bool        activeRequest;
    bool        activeEnable;
}heaterPair_t;

bool heaterInit(void);
bool heaterControl(heater_t* pHeater, bool enable);
bool heaterOvercurrent(heater_t* pHeater, bool* pOC);
bool heaterGetCurrent(heater_t* pHeater, uint16_t* current_mA);

bool heaterPairRequest(heaterPair_t* pHeaterPair, bool enable);
bool heaterPairControl(heaterPair_t* pHeaterPair, bool enable);

#endif //HEATER_H_
