/*
 * adc.h
 *
 *  Created on: May 5, 2015
 *      Author: Mike
 */

#ifndef ADC_H_
#define ADC_H_

#include "R_PG_BoardBringup.h"
#include "initFlags.h"
#include "r_pdl_intc.h"
#include "r_pdl_definitions.h"

#define adcBufferDepth  5

typedef enum adcChannel_e
{
    eAdcChannel0 = 0,
    eAdcChannel1,
    eAdcChannel2,
    eAdcChannel3,
    eAdcChannel4,
    eAdcChannel5,
    eAdcChannel6,
    eAdcChannel7,
    eAdcNumChannels,
} adcChannel_t;

typedef struct adc_s
{
    adcChannel_t channel;
} adc_t;

bool adcInit(void);
bool adcStart(void);
bool adcStop(void);
bool adcReadFiltered(adc_t* pAdc, uint16_t* pValue);
bool adcReadRaw(adc_t* pAdc, uint16_t* pBuffer, uint16_t bufferSize);

#endif /* ADC_H_ */
