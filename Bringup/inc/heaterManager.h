/*
 * heaterManager.h
 *
 *  Created on: Jul 27, 2015
 *      Author: Mike
 */

#ifndef HEATERMANAGER_H_
#define HEATERMANAGER_H_

#include "NextGen_Quad_Conserver_Brd.h"
#include "heater.h"
#include "Queue.h"

bool heaterManagerInit(void);
bool heaterManagerAddHeater(heaterPair_t* pHeaterPair);
bool manageActiveHeaters(void);
bool manageDisabledHeaters(heaterPair_t*);

#endif /* HEATERMANAGER_H_ */
