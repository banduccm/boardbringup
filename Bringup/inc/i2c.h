/*
 * i2c.h
 *
 *  Created on: May 15, 2015
 *      Author: Mike
 */

#ifndef I2C_H_
#define I2C_H_

#include "R_PG_BoardBringup.h"
#include "initFlags.h"
#include "ADT7410.h"

#define I2C_READ_MASK			0x01
#define I2C_WRITE_MASK			0x00

typedef enum {
    eI2cDeviceTemperature = (0x48 << 1),   //7-bit I2C address for the ADT7410TRZ temperature sensor (U8)
    eI2cDeviceDisplay = 0x50,       		//7-bit I2C address for the NHD-0220D3Z-FL-GBW-V3 Display (J3)
    eI2cDeviceInvalid
} i2cDevice_t;

bool i2cInit(void);
bool i2cTransmit(i2cDevice_t device, uint8_t* data, uint16_t dataSize);
bool i2cTransmitWithoutStop(i2cDevice_t device, uint8_t* data, uint16_t dataSize);
bool i2cReceive(i2cDevice_t device, uint8_t* data, uint16_t dataSize);
bool i2cRestartReceive(i2cDevice_t device, uint8_t* data, uint16_t dataSize);

#endif /* I2C_H_ */
