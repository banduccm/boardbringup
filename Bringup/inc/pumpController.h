/*
 * pumpController.h
 *
 *  Created on: Jul 19, 2015
 *      Author: Mike
 */

#ifndef PUMPCONTROLLER_H_
#define PUMPCONTROLLER_H_

#include "pump.h"

typedef struct pumpController_s
{
    //Pumps
    pump_t   inletPump;
    pump_t   returnPump;

    //GPIOs
    gpio_t      detect;
    gpio_t      highLevelFloat;
    gpio_t      lowLevelFloat;
    gpio_t      inletHighPressure;

    //Status
    bool detectStatus;
    bool highLevelFloatStatus;
    bool lowLevelFloatStatus;
    bool inletHighPressureStatus;
} pumpController_t;

bool pumpControllerInit(void);
bool pumpControllerDetect(pumpController_t*, bool*);
bool pumpControllerReadState(pumpController_t*);


#endif /* PUMPCONTROLLER_H_ */
