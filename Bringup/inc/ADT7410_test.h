/*
 * ADT7410_test.h
 *
 *  Created on: Aug 26, 2015
 *      Author: Stephen
 */

#ifndef ADT7410_TEST_H_
#define ADT7410_TEST_H_

#include "i2c.h"
#include "ADT7410.h"

bool adt7410test(void);

#endif /* ADT7410_TEST_H_ */
