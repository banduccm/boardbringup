/*
 * Queue.h
 *
 *  Created on: Aug 15, 2015
 *      Author: Mike
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include "NextGen_Quad_Conserver_Brd.h"

typedef struct queue_s{
	uint32_t elements;
	uint32_t filledElements;
	uint32_t* queue;
	uint32_t queueIn;
	uint32_t queueOut;
}queue_t;

// A simple circular Queue. Lovingly stolen from https://stackoverflow.com/a/215575
bool queueInit(queue_t*, uint32_t);
void queueDeinit(queue_t* pQueue);
bool queuePeek(queue_t*, uint32_t*);
bool queueFind(queue_t*, uint32_t);
bool enqueue(queue_t*, uint32_t);
bool dequeue(queue_t*, uint32_t*);

#endif /* QUEUE_H_ */
