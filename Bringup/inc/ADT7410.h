/*
 * ADT7410.h
 *
 *  Created on: Aug 5, 2015
 *      Author: Stephen
 */

#ifndef ADT7410_H_
#define ADT7410_H_

#include "R_PG_BoardBringup.h"
#include "i2c.h"

#define ADT7410_ADDR				(0x48 << 1)

#define ADT7410_REG					uint8_t
#define ADT7410_DATA				uint8_t

//ADT7410 register defines
#define ADT7410_TEMP_MSB_REG				0x00
#define ADT7410_TEMP_LSB_REG				0x01
#define ADT7410_STATUS_REG					0x02
#define ADT7410_CONFIG_REG					0x03
#define ADT7410_T_HIGH_SET_MSB_REG			0x04
#define ADT7410_T_HIGH_SET_LSB_REG			0x05
#define ADT7410_T_LOW_SET_MSB_REG			0x06
#define ADT7410_T_LOW_SET_LSB_REG			0x07
#define ADT7410_T_CRIT_SET_MSB_REG			0x08
#define ADT7410_T_CRIT_SET_LSB_REG			0x09
#define ADT7410_T_HYST_SET_REG				0x0A
#define ADT7410_ID_REG						0x0B
#define ADT7410_SW_RESET_REG				0x2F

//Status register mask defines
#define STATUS_REG_T_LOW			0x10
#define STATUS_REG_T_HIGH			0x20
#define STATUS_REG_T_CRIT			0x40
#define STATUS_REG_RDY_N			0x80

//Configuration register mask defines
#define CONFIG_REG_NUM_FAULTS		0x03
#define CONFIG_REG_CT_POL			0x04
#define CONFIG_REG_INT_POL			0x08
#define CONFIG_REG_INT_CT_MODE		0x10
#define CONFIG_REG_OP_MODE			0x60
#define CONFIG_REG_RESOLUTION		0x80

//Values
#define MAX_TEMP_SET				150
#define MIN_TEMP_SET				0

//System defines
#define TEMP_16_BITS				1
#define TEMP_13_BITS				0

//System setup values
#define NUM_TEMP_BITS				TEMP_16_BITS

//Internal functions
bool adt7410ReadRegister(ADT7410_REG addr, ADT7410_DATA* data);
bool adt7410WriteRegister(ADT7410_REG addr, ADT7410_DATA* data);

bool adt7410ConvertTempToRegisterValue(double temperature, uint16_t* retVal);
bool adt7410ConvertRegisterValueToDouble(uint16_t regVal, double* temp);

//API functions
bool adt7410GetStatus(ADT7410_DATA* data);
bool adt7410GetConfig(ADT7410_DATA* data);
bool adt7410SetConfig(ADT7410_DATA* data);
bool adt7410SetHighTemp(double temperature);
bool adt7410SetLowTemp(double temperature);
bool adt7410SetCritTemp(double temperature);
bool adt7410SetHysteresis(int hysteresis);
bool adt7410GetID(ADT7410_DATA* id);
bool adt7410swReset(void);

bool adt7410GetTemperature(double* temp);




#endif /* ADT7410_H_ */
