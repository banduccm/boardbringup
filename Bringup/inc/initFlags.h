/*
 * initFlags.h
 *
 *  Created on: May 27, 2015
 *      Author: Mike
 */

#ifndef INITFLAGS_H_
#define INITFLAGS_H_

typedef struct
{
   uint8_t gpio;
   uint8_t adc;
   uint8_t spi;
   uint8_t uart;
   uint8_t i2c;
}initFlags_t;

extern initFlags_t initFlags;

#endif /* INITFLAGS_H_ */
