/*
 * conserver.h
 *
 *  Created on: May 5, 2015
 *      Author: Mike
 */

#ifndef CONSERVER_H_
#define CONSERVER_H_

#include "heater.h"
#include "pumpController.h"

typedef struct conserver_s
{
	//Heaters
    heaterPair_t    heaters;

    //GPIOs
    gpio_t      detect;
    gpio_t      highTempSwitch;
    gpio_t      lowPressureSwitch;
    gpio_t      overtempSwitch;

    //Status
    bool		detectStatus;
    bool 		highTempStatus;
    bool		lowPressureStatus;
    bool		overTempStatus;
} conserver_t;

bool conserverInit(void);
bool conserverDetect(conserver_t*, bool*);
bool conserverDisableInactiveHeaters(conserver_t* pConserver);
bool conserverReadState(conserver_t*);

#endif /* CONSERVER_H_ */
