/*
 * controlTasks.h
 *
 *  Created on: Jul 19, 2015
 *      Author: Mike
 */

#ifndef CONTROLTASKS_H_
#define CONTROLTASKS_H_

#include "conserver.h"

bool conserverTask(conserver_t* pConserver);

#endif /* CONTROLTASKS_H_ */
