/*
 * spi.h
 *
 *  Created on: May 11, 2015
 *      Author: Mike
 */

#ifndef SPI_H_
#define SPI_H_

#include "R_PG_BoardBringup.h"
#include "initFlags.h"

typedef enum {
    eSpiDeviceWiFi = 0,
    eSpiDeviceFlash = 1,
    eSpiDeviceInvalid
}spiDevice_t;

bool spiInit(void);
bool spiTransfer(spiDevice_t device, uint32_t* txData, uint32_t* rxData, uint32_t count);


#endif /* SPI_H_ */
