/*
 * crc.h
 *
 *  Created on: Sep 1, 2015
 *      Author: Mike
 */

#ifndef CRC_H_
#define CRC_H_

#include "R_PG_BoardBringup.h"
#include "initFlags.h"

bool crcOpen(void);
bool crcClose(void);
bool crcComputeByte(uint8_t, uint16_t*);
bool crcComputeBlock(uint8_t*, uint32_t, uint16_t*);

#endif /* CRC_H_ */
