/*
 * controlObjects.h
 *
 *  Created on: Jul 20, 2015
 *      Author: Mike
 */

#ifndef CONTROLOBJECTS_H_
#define CONTROLOBJECTS_H_

#include "conserver.h"
#include "NextGen_Quad_Conserver_Brd.h"

static conserver_t conserverOne = {{{{CNS1_MAIN_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS1_MAIN_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel0}, false},
                                   {{CNS1_WRAP_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS1_WRAP_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel1}, false}},
                                   {CNS1_DETECT_PIN, eGpioPolarityActiveLow},
                                   {CNS1_HIGHTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS1_LOWPRESSURE_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS1_OVERTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   false, false, false, false };

static conserver_t conserverTwo = {{{{CNS2_MAIN_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS2_MAIN_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel2}, false},
                                   {{CNS2_WRAP_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS2_WRAP_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel3}, false}},
                                   {CNS2_DETECT_PIN, eGpioPolarityActiveLow},
                                   {CNS2_HIGHTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS2_LOWPRESSURE_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS2_OVERTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   false, false, false, false };

static conserver_t conserverThree ={{{{CNS3_MAIN_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS3_MAIN_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel4}, false},
                                   {{CNS3_WRAP_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS3_WRAP_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel5}, false}},
                                   {CNS3_DETECT_PIN, eGpioPolarityActiveLow},
                                   {CNS3_HIGHTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS3_LOWPRESSURE_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS3_OVERTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   false, false, false, false };

static conserver_t conserverFour = {{{{CNS4_MAIN_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS4_MAIN_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel6}, false},
                                   {{CNS4_WRAP_HEATER_ENABLE_PIN, eGpioPolarityActiveLow}, {CNS4_WRAP_HEATER_OVERCUR_PIN, eGpioPolarityActiveHigh}, {eAdcChannel7}, false}},
                                   {CNS4_DETECT_PIN, eGpioPolarityActiveLow},
                                   {CNS4_HIGHTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS4_LOWPRESSURE_SWITCH_PIN, eGpioPolarityActiveLow},
                                   {CNS4_OVERTEMP_SWITCH_PIN, eGpioPolarityActiveLow},
                                   false, false, false, false };


#endif /* CONTROLOBJECTS_H_ */
