/*
 * spiFlash.h
 *
 *  Created on: Sep 9, 2015
 *      Author: Mike
 */

#ifndef SPIFLASH_H_
#define SPIFLASH_H_

#include "R_PG_BoardBringup.h"
#include "spi.h"

bool spiFlashInit(void);
bool spiFlashWrite(uint32_t address, uint8_t* pData, uint32_t bytes);
bool spiFlashRead(uint32_t address, uint8_t* pData, uint32_t bytes);

#endif /* SPIFLASH_H_ */
