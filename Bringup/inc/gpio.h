/*
 * gpio.h
 *
 *  Created on: May 5, 2015
 *      Author: Mike
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "R_PG_BoardBringup.h"
#include "initFlags.h"

typedef enum gpioState_e
{
    eGpioStateActive = 0,
    eGpioStateInactive,
    eGpioStateInvalid,
} gpioState_t;

typedef enum gpioPolarity_e
{
    eGpioPolarityActiveHigh = 0,
    eGpioPolarityActiveLow,
    eGpioPolarityInvalid,
} gpioPolarity_t;

typedef struct gpio_s
{
    uint16_t port;
    gpioPolarity_t polarity;
} gpio_t;

bool gpioInit(void);
bool gpioWrite(gpio_t*, gpioState_t);
bool gpioRead(gpio_t*, gpioState_t*);

#endif /* GPIO_H_ */
