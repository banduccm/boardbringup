/*
 * M25P20_SPIFlash.h
 *
 *  Created on: Sep 9, 2015
 *      Author: Mike
 */

#ifndef M25P20_SPIFlash_H_
#define M25P20_SPIFlash_H_

// Address:
// | b31 | b30 | b29 | b28 | b27 | b26 | b25 | b24 | b23 | b22 | b21 | b20 | b19 | b18 | b17 | b16 | b15 | b14 | b13 | b12 | b11 | b10 | b9  | b8  | b7  | b6  | b5  | b4  | b3  | b2  | b1  | b0  |
// |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  |  x  | PA  | PA  | PA  | PA  | PA  | PA  | PA  | PA  | PA  | PA  | BA  | BA  | BA  | BA  | BA  | BA  | BA  | BA  |

/*
 * Command set for the M25P20 SPI Flash
 */
#define WREN		0x06	/*!< Write Enable */
#define WRDI		0x04	/*!< Write Disable */
#define RDID		0x9F	/*!< Read Identification */
#define RDSR		0x05	/*!< Read Status Register */
#define WRSR		0x01	/*!< Write Status Register */
#define READ		0x03	/*!< Read Data Bytes */
#define	FAST_READ	0x0B	/*!< Fast Read Data */
#define	PP			0x02	/*!< Page Program */
#define SE			0xD8	/*!< Sector Erase */
#define BE			0xC7	/*!< Bulk Erase */
#define DP			0xB9	/*!< Deep Power-down */
#define RES			0xAB	/*!< Reset; Release from Deep Power-down */

/*
 * Status Register Bit Masks
 */
#define WIP		1<<0	/*!< Write in Progress bit */
#define WEL		1<<1	/*!< Write Enable Latch bit */
#define BP0		1<<2	/*!< Block Protect bit 0 */
#define BP1		1<<3	/*!< Block Protect bit 1 */
#define SRWD	1<<7	/*!< Status Register Write Disable bit */

/*
 * Chip Properties
 */
#define PAGE_SIZE_BYTES	256
#define NUM_PAGE		1024

/*
 * Miscellaneous Defines
 */
#define INSTRUCTION_LENGTH_BYTES	1	/*!< Instructions are 1 byte long */
#define ADDRESS_LENGTH_BYTES		3	/*!< The M25P20 uses 24-bit addresses */
#define ADDRESS_MASK				0x003FFFFF
#define PAGE_START_MASK				0x003FFF00
#define BYTE_OFFSET_MASK			0x000000FF

#endif /* M25P20_SPIFlash_H_ */
