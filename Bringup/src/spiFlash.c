/*
 * spiFlash.c
 *
 *  Created on: Sep 9, 2015
 *      Author: Mike
 */

#include <stddef.h>
#include "spiFlash.h"
#include "M25P20_SPIFlash.h"

/* Local Functions
 *
 */
bool spiFlashWriteEnable(void);

bool spiFlashInit(void)
{
	bool success = true;

	return success;
}

bool spiFlashWrite(uint32_t address, uint8_t* pData, uint32_t bytes)
{
	bool success = false;
	uint32_t i = 0;
	uint32_t bytesRemaining = bytes;
	uint32_t bytesToWrite = 0;
	uint8_t* pDataStart = pData;
	uint32_t writeAddress = address;
	uint32_t page_address = address & PAGE_START_MASK;
	uint32_t byte_offset = address & BYTE_OFFSET_MASK;
	uint32_t* pTxData;

	do{
		//Determine if the write will exceed page boundaries
		if(PAGE_SIZE_BYTES > (bytesRemaining + byte_offset))
		{
			//Write the number of bytes left on this page
			bytesToWrite = PAGE_SIZE_BYTES - byte_offset;
		}
		// If this write will fit on a single page:
		else if(PAGE_SIZE_BYTES <= (bytesRemaining + byte_offset))
		{
			bytesToWrite = bytesRemaining;
		}

		if(0 < bytesToWrite)
		{
			//Allocate Buffer
			pTxData = (uint32_t*)calloc(INSTRUCTION_LENGTH_BYTES + ADDRESS_LENGTH_BYTES + bytesToWrite, sizeof(uint32_t));

			if(NULL != pTxData)
			{
				// 1) Sent a WREN command then release the chip enable pin
				success = spiFlashWriteEnable();

				// 2) Prepare the transmit buffer with the PP command and 3-byte address
				pTxData[0] = PP;
				pTxData[1] = writeAddress & 0x000000FF;
				pTxData[2] = writeAddress & 0x0000FF00;
				pTxData[3] = writeAddress & 0x003F0000;	/* Bytes 23-18 are don't care */

				// 3) Copy the transmit data into a properly-data-sized buffer
				for(i = 0; i < bytesToWrite; i++)
				{
					pTxData[INSTRUCTION_LENGTH_BYTES + ADDRESS_LENGTH_BYTES + i] = pDataStart[i];
				}

				// 4) Start the SPI transfer
				success = spiTransfer(eSpiDeviceFlash, pTxData, pTxData, (INSTRUCTION_LENGTH_BYTES + ADDRESS_LENGTH_BYTES + bytesToWrite));

				// 5) Delete the buffer
				free(pTxData);

				// 6) Update pointers and indexes
				bytesRemaining -= bytesToWrite;	//Subtract the number of bytes written from the remaining bytes to write
				pDataStart += bytesToWrite;		//Move the data pointer to the next byte to be written
				writeAddress += bytesToWrite;	//Move the address pointer to the next byte to be written
			}
		}
	} while(0 < bytesRemaining);

	return success;
}

bool spiFlashRead(uint32_t address, uint8_t* pData, uint32_t bytes)
{
	bool success = false;
	uint32_t i = 0;

	//Allocate buffer
	uint32_t* pTxData;
	pTxData = (uint32_t*)calloc(INSTRUCTION_LENGTH_BYTES + ADDRESS_LENGTH_BYTES + bytes, sizeof(uint32_t));

	if(NULL != pTxData)
	{
		//Perform a read! (See Section 6.6)

		// 1) Prep the transmit buffer with the Read command and 3-byte address
		pTxData[0] = READ;
		pTxData[1] = address & 0x000000FF;
		pTxData[2] = address & 0x0000FF00;
		pTxData[3] = address & 0x003F0000;	/* Bytes 23-18 are don't care */

		// 2) Kick off the SPI transfer
		success = spiTransfer(eSpiDeviceFlash, pTxData, pTxData, (INSTRUCTION_LENGTH_BYTES + ADDRESS_LENGTH_BYTES + bytes));

		// 3) Move the data into the byte array
		for(i = 0; i < bytes; i++)
		{
			pData[i] = pTxData[i + (ADDRESS_LENGTH_BYTES + INSTRUCTION_LENGTH_BYTES)];
		}

		// 4) Delete the buffer
		free(pTxData);
	}

	return success;
}

bool spiFlashWriteEnable(void)
{
	uint32_t txData = WREN;
	return spiTransfer(eSpiDeviceFlash, &txData, &txData, 1);
}

