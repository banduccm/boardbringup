/*
 * adc.c
 *
 *  Created on: May 6, 2015
 *      Author: Mike
 */

#include "adc.h"

//Flag to let us know that the adc buffers have been filled
static volatile bool adcInitialReadings = false;

//2D buffer to store ADC samples
uint16_t adcBuffer[adcBufferDepth][eAdcNumChannels];

//**********************************************************/
// Local Functions:

uint16_t adcFilter(uint16_t channel);
void bubble_sort (uint16_t *a, uint16_t n);

/*! \fn uint16_t adcFilter(uint16_t channel)
 *  \brief Defines the filter used for the adcReadFiltered routine. Current a simple median filter.
 *  \param channel - the ADC channel to filter
 *  \return The filtered ADC value, 12-bits right-aligned.
 */
uint16_t adcFilter(uint16_t channel)
{
    uint16_t i = 0;
    uint16_t output = 0;
    uint16_t dataArray[adcBufferDepth] = {0};

    if(adcInitialReadings)
    {
        for(i = 0; i < adcBufferDepth; i++)
        {
            dataArray[i] = adcBuffer[i][channel];
        }

        bubble_sort(dataArray, adcBufferDepth);
        output = dataArray[(adcBufferDepth - 1) / 2];
    }

    return output;
}

// From http://rosettacode.org/wiki/Bubble_Sort#C with love <3
/*! \fn void bubble_sort(uint16_t* a, uint16_t n)
 *  \brief Performs a basic bubble sort
 *  \param *a - pointer to the array of values to be sorted
 *  \param n - number of elements in the given array
 */
void bubble_sort (uint16_t *a, uint16_t n)
{
    uint16_t i, t, s = 1;
    while (s) {
        s = 0;
        for (i = 1; i < n; i++) {
            if (a[i] < a[i - 1]) {
                t = a[i];
                a[i] = a[i - 1];
                a[i - 1] = t;
                s = 1;
            }
        }
    }
}

//**********************************************************/
// Global Functions:

/*! \fn bool adcInit(void)
 *  \brief Initializes the ADC hardware and configures the ADC interrupt
 *  \return success. False if the ADC init fails
 */
bool adcInit(void)
{
    bool retVal = true;

    if(!initFlags.adc)
    {   //Only initialize the ADC if it has not been inited yet
        retVal = R_PG_ADC_12_Set_S12AD0();

        if(retVal)
        {   //Enable ADC12 interrupt:
            //Set priority:
            retVal = R_INTC_Write(PDL_INTC_REG_IPR_S12AD_S12ADI, 5);

            if(retVal)
            {   //Enable ADC12 interrupt with INTC
                retVal = R_INTC_Write(PDL_INTC_REG_IER0C, 6);
            }
        }

        if(retVal)
        {   //If the initialization is successful, set the init flag
            initFlags.adc = 1;
        }
    }

    return retVal;
}

/*! \fn bool adcStart(void)
 *  \brief Starts ADC conversions
 *  \return success. False if the conversion enable fails.
 */
bool adcStart(void)
{
    return R_PG_ADC_12_StartConversionSW_S12AD0();
}

/*! \fn bool adcStop(void)
 *  \brief Stops ADC conversions
 *  \return success. False if the conversion disable fails
 */
bool adcStop(void)
{
    return R_PG_ADC_12_StopConversion_S12AD0();
}

/*! \fn bool adcReadFiltered(adc_t* pAdc, uint16_t* pValue)
 *  \brief Reads a single ADC value for the given channel filtered according to the adcFilter function
 *  \param [in] pAdc - pointer to the ADC object to be read
 *  \param [out] pValue - pointer to a uint16_t where the result will be stored
 *  \return success. False if the given ADC channel is invalid
 */
bool adcReadFiltered(adc_t* pAdc, uint16_t* pValue)
{
    bool retVal = true;

    if(pAdc != 0)
    {
        *pValue = adcFilter(pAdc->channel);
    }
    else
    {
        retVal = false;
    }

    return retVal;
}

/*! \fn bool adcReadRaw(adc_t* pAdc, uint16_t* pBuffer, uint16_t bufferSize)
 *  \brief Reads all of the current ADC values for the given channel
 *  \param [in] pAdc - pointer to the ADC object to be read
 *  \param [out] pBuffer - buffer into which the ADC values will be written
 *  \param [in] bufferSize - number of elements in pBuffer
 *  \return success. False if an error occurs
 */
bool adcReadRaw(adc_t* pAdc, uint16_t* pBuffer, uint16_t bufferSize)
{
    uint16_t i = 0;
    bool retVal = true;

    if(pAdc != 0)
    {
        if(bufferSize >= adcBufferDepth)
        {
            for(i = 0; i < adcBufferDepth; i++)
            {
                pBuffer[i] = adcBuffer[i][pAdc->channel];
            }
        }
        else
        {
            retVal = false;
        }
    }
    else
    {
        retVal = false;
    }

    return retVal;
}

/*! \fn void S12ad0IntFunc(void)
 *  \brief Interrupt for the 12-bit ADC. This ISR is fired when the ADC has completed a read of each channel.
 *  \detail The ADC interrupt is fired when a new value has been read for each of the active ADC channels.
 *          The ISR copies the values from the ADC to a circular buffer for each channel.
 */
void S12ad0IntFunc(void)
{
    static uint16_t adcBufferIndex = 0; //Index to keep track of which buffer to fill next
    uint16_t* adcReadings = 0;          //Pointer to simplify the adc_getresult call
    bool retVal = true;

    adcReadings = adcBuffer[adcBufferIndex];

    retVal = R_PG_ADC_12_GetResult_S12AD0(adcReadings);

    if(retVal)
    {   //If the reading is successful, move to the next buffer index
        if(adcBufferIndex >= adcBufferDepth)
        {   //Act like a circular buffer; roll over to overwrite when we hit the max number of samples
            adcBufferIndex = 0;
            adcInitialReadings = true;
        }
        else
        {
            adcBufferIndex+=1;
        }
    }
}
