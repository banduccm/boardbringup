/*
 * Queue.c
 *
 *  Created on: Aug 15, 2015
 *      Author: Mike
 */

#include "Queue.h"

/* Very simple queue
 * These are FIFO queues which discard the new data when full.
 *
 * Queue is empty when in == out.
 * If in != out, then
 *  - items are placed into in before incrementing in
 *  - items are removed from out before incrementing out
 * Queue is full when in == (out-1 + QUEUE_SIZE) % QUEUE_SIZE;
 *
 * The queue will hold QUEUE_ELEMENTS number of items before the
 * calls to QueuePut fail.
 */

bool queueInit(queue_t* pQueue, uint32_t size)
{
	bool success = true;

    pQueue->queueIn = 0;
    pQueue->queueOut = 0;
    pQueue->elements = size + 1;	//The queue design always has an empty slot; it makes for easier full/empty checking
    pQueue->filledElements = 0;
    pQueue->queue = (uint32_t*)calloc(pQueue->elements, sizeof(uint32_t));

    if(NULL == pQueue->queue)
    {
    	success = false;
    }

    return success;
}

void queueDeinit(queue_t* pQueue)
{
	pQueue->elements = 0;
	pQueue->queueIn = 0;
	pQueue->queueOut = 0;
	free(pQueue->queue);
	pQueue->queue = NULL;
}

bool queuePeek(queue_t* pQueue, uint32_t* peek)
{	//Look but don't touch
	bool success = true;

    if(pQueue->queueIn == pQueue->queueOut)
    {	/* Queue Empty - nothing to get*/
    	success = false;
    }

    if(success)
    {
		*peek = pQueue->queue[pQueue->queueOut];
    }

    return success;
}

bool queueFind(queue_t* pQueue, uint32_t element)
{
	//@TODO: This should probably be made to search only "active" locations in the Queue's array.
	bool found = false;
	uint32_t i = 0;

	for(i = 0; i < pQueue->elements; i++)
	{
		if(pQueue->queue[i] == element)
		{
			found = true;
			break;
		}
	}

	return found;
}

bool enqueue(queue_t* pQueue, uint32_t new)
{
	bool success = true;
    if(pQueue->queueIn == ((pQueue->queueOut - 1 + pQueue->elements) % (pQueue->elements)))
    {	/* Queue Full*/
    	success = false;
    }

    if(success)
    {
		pQueue->queue[pQueue->queueIn] = new;
		pQueue->queueIn = (pQueue->queueIn + 1) % (pQueue->elements);
		pQueue->filledElements += 1;
    }

    return success;
}

bool dequeue(queue_t * pQueue, uint32_t *old)
{
	bool success = true;

    if(pQueue->queueIn == pQueue->queueOut)
    {	/* Queue Empty - nothing to get*/
    	success = false;
    }

    if(success)
    {
		*old = pQueue->queue[pQueue->queueOut];
		pQueue->queue[pQueue->queueOut] = NULL;	//Clear out the element
		pQueue->queueOut = (pQueue->queueOut + 1) % (pQueue->elements);
		pQueue->filledElements -= 1;
    }

    return success;
}
