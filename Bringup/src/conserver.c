/*
 * conserver.c
 *
 *  Created on: May 6, 2015
 *      Author: Mike
 */

#include "conserver.h"

/*! \fn bool conserverInit(void)
 *  \brief Initializes all hardware interfaces used by the Conserver class
 *  \return success. False if any of the hardware initializations fail
 */
bool conserverInit(void)
{
    bool retVal = false;

    //gpioInit is called by heaterInit
    retVal = heaterInit();

    return retVal;
}

/*! \fn bool conserverDetect(conserver_t* pConserver, bool* present)
 *  \brief Polls the given conserver's detect pin to determine if the conserver hardware is attached
 *  \param [in] pConserver - The conserver to detect
 *  \param [out] present - State of the given conserver's detect pin
 *  \return success. False if the GPIO read fails
 */
bool conserverDetect(conserver_t* pConserver, bool* present)
{
    gpioState_t detectState = eGpioStateInactive;
    bool retVal = true;

    retVal = gpioRead(&(pConserver->detect), &detectState);

    if(retVal)
    {
        if(detectState == eGpioStateActive)
        {
            *present = true;
        }
        else
        {
            *present = false;
        }
    }

    return retVal;
}

/*! \fn bool conserverDisableInactiveHeaters(conserver_t* pConserver)
 *  \brief Iterates through the given conserver's heaters and handles any that have been disabled. Should be run periodically.
 *  \param pConserver - The conserver to manage
 *  \return success. False if the conserver or heaters are invalid
 */
bool conserverDisableInactiveHeaters(conserver_t* pConserver)
{
    bool success = false;

    if(NULL != &(pConserver->heaters))
    {
        success = manageDisabledHeaters(&(pConserver->heaters));
    }

    return success;
}

/*! \fn bool conserverReadState(conserver_t* pConserver)
 *  \brief Reads and updates the switch states for the detect, high temp, low pressure, and overtemp
 *  \param pConserver - The conserver to manage
 *  \return success. False if any of the GPIO reads fail
 */
bool conserverReadState(conserver_t* pConserver)
{
	bool retVal = true;
	bool status = false;
	gpioState_t detectState = eGpioStateInvalid;

	//Read detect
	status = gpioRead(&pConserver->detect, &detectState);
	if(true == status)
	{
		pConserver->detectStatus = detectState;
	}
	else
	{
		retVal = false;
	}

	//Read high temp switch
	status = gpioRead(&pConserver->highTempSwitch, &detectState);
	if(true == status)
	{
		pConserver->highTempStatus = detectState;
	}
	else
	{
		retVal = false;
	}

	//Read low pressure switch
	status = gpioRead(&pConserver->lowPressureSwitch, &detectState);
	if(true == status)
	{
		pConserver->lowPressureStatus = detectState;
	}
	else
	{
		retVal = false;
	}

	//Read overtemp switch
	status = gpioRead(&pConserver->overtempSwitch, &detectState);
	if(true == status)
	{
		pConserver->overTempStatus = detectState;
	}
	else
	{
		retVal = false;
	}

	return retVal;
}//endConserverReadSwitchStates()
