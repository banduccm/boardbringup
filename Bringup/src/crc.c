/*
 * crc.c
 *
 *  Created on: Sep 1, 2015
 *      Author: Mike
 */

#include "crc.h"

bool crcOpen(void)
{
	bool success = true;

	success = R_PG_CRC_Set();

	return success;
}

bool crcClose(void)
{
	bool success = true;

	success = R_PG_CRC_StopModule();

	return success;
}

bool crcComputeByte(uint8_t data, uint16_t* pResult)
{
	bool success = false;

	success = R_PG_CRC_InputData(data);

	if(success)
	{
		success = R_PG_CRC_GetResult(pResult);
	}

	return success;
}

bool crcComputeBlock(uint8_t* buffer, uint32_t bufferSize, uint16_t* pResult)
{
	bool success = true;
	uint16_t i = 0;

	for(i = 0; (i < bufferSize) && (success); i ++)
	{
		success = R_PG_CRC_InputData(buffer[i]);
	}

	if(success)
	{
		success = R_PG_CRC_GetResult(pResult);
	}

	return success;
}
