/*
 * pumpController.c
 *
 *  Created on: Jul 19, 2015
 *      Author: Mike
 */

#include "pumpController.h"

bool pumpControllerInit(void)
{
    bool retVal = false;

    //gpioInit is called by pumpInit
    retVal = pumpInit();

    return retVal;
}

bool pumpControllerDetect(pumpController_t* pPumpController, bool* present)
{
    gpioState_t detectState = eGpioStateInactive;
    bool retVal = true;

    retVal = gpioRead(&(pPumpController->detect), &detectState);

    if(retVal)
    {
        if(detectState == eGpioStateActive)
        {
            *present = true;
        }
        else
        {
            *present = false;
        }
    }

    return retVal;
}

bool pumpControllerReadState(pumpController_t* pPumpController)
{
    bool retVal = true;
    bool status = false;
    gpioState_t detectState = eGpioStateInvalid;

    //Read detect
    status = gpioRead(&pPumpController->detect, &detectState);
    if(true == status)
    {
        pPumpController->detectStatus = detectState;
    }
    else
    {
        retVal = false;
    }

    //Read high level float switch
    status = gpioRead(&pPumpController->highLevelFloat, &detectState);
    if(true == status)
    {
        pPumpController->highLevelFloatStatus = detectState;
    }
    else
    {
        retVal = false;
    }

    //Read low level float switch
    status = gpioRead(&pPumpController->lowLevelFloat, &detectState);
    if(true == status)
    {
        pPumpController->lowLevelFloatStatus = detectState;
    }
    else
    {
        retVal = false;
    }

    //Read inlet high pressure switch
    status = gpioRead(&pPumpController->inletHighPressure, &detectState);
    if(true == status)
    {
        pPumpController->inletHighPressureStatus = detectState;
    }
    else
    {
        retVal = false;
    }

    return retVal;
}
