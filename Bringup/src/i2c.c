/*
 * i2c.c
 *
 *  Created on: May 15, 2015
 *      Author: Mike
 */

#include "i2c.h"

bool i2cInit()
{
    bool retVal = true;

    if(!initFlags.i2c)
    {
        retVal = R_PG_SCI_Set_C6();
    }

    if(retVal)
    {
        initFlags.i2c = 1;
    }

    return retVal;
}

bool i2cTransmit(i2cDevice_t device, uint8_t* data, uint16_t dataSize)
{
	return R_PG_SCI_I2CMode_Send_C6(0, (uint16_t) device, data, dataSize);
}

bool i2cTransmitWithoutStop(i2cDevice_t device, uint8_t* data, uint16_t dataSize)
{
	return R_PG_SCI_I2CMode_SendWithoutStop_C6(0, (uint16_t) device, data, dataSize);
}

bool i2cReceive(i2cDevice_t device, uint8_t* data, uint16_t dataSize)
{
    return R_PG_SCI_I2CMode_Receive_C6(0, (uint16_t) device, data, dataSize);
}

bool i2cRestartReceive(i2cDevice_t device, uint8_t* data, uint16_t dataSize)
{
    return R_PG_SCI_I2CMode_RestartReceive_C6(0, (uint16_t) device, data, dataSize);
}
