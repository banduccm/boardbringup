/*
 * testContainer.c
 *
 *  Created on: Aug 3, 2015
 *      Author: Mike
 */

#include "testContainer.h"

void testHeaterController(void)
{
    //Declare mock objects for testing
    heaterPair_t testHeaters[4];

    //Prepare the heater manager
    assert("Heater Manager Init", heaterManagerInit());

//Test operation with one heater:

    //Add one of the heaters to the pile
    assert("Add first heater", heaterManagerAddHeater(&(testHeaters[0])));

    //Set all our pins and flags to off:
    disableAll(&testHeaters[0]);

    testHeaters[0].activeRequest = true;

    //Run the heater management logic
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], true);

    //Disable the heater's active request
    testHeaters[0].activeRequest = false;

    //Run the disable inactive heaters call
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[0]));

    //Expect all heaters to be off:
    checkHeaterEnableStatus(&testHeaters[0], false);

//Test operation with two heaters:

    assert("Add second heater", heaterManagerAddHeater(&(testHeaters[1])));

    //Make sure both heaters are off:
    disableAll(&testHeaters[0]);
    disableAll(&testHeaters[1]);

    testHeaters[0].activeRequest = true;
    testHeaters[1].activeRequest = true;

    //Run the heater management logic
    manageActiveHeaters();

    //Expect the first two heater pairs to be active:
    checkHeaterEnableStatus(&testHeaters[0], true);
    checkHeaterEnableStatus(&testHeaters[1], true);

    //Disable the heater's active request
    testHeaters[0].activeRequest = false;
    testHeaters[1].activeRequest = false;

    //Run the disable inactive heaters call
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[0]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[1]));

    //Expect all heaters to be off:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], false);

//Test operation with Three heaters (this is where it gets interesting):

    assert("Add Third Heater", heaterManagerAddHeater(&(testHeaters[2])));

    //Make sure all heaters are off
    disableAll(&testHeaters[0]);
    disableAll(&testHeaters[1]);
    disableAll(&testHeaters[2]);

    testHeaters[0].activeRequest = true;
    testHeaters[1].activeRequest = true;
    testHeaters[2].activeRequest = true;

    //Run the heater management logic
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], true);
    checkHeaterEnableStatus(&testHeaters[1], true);
    checkHeaterEnableStatus(&testHeaters[2], false);

    //Run the management logic again to switch heaters
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], true);
    checkHeaterEnableStatus(&testHeaters[2], true);

    //Disable the heater's active request
    testHeaters[0].activeRequest = false;
    testHeaters[1].activeRequest = false;
    testHeaters[2].activeRequest = false;

    //Run the disable inactive heaters call
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[0]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[1]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[2]));

    //Expect all heaters to be off:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], false);
    checkHeaterEnableStatus(&testHeaters[2], false);

//Test operation with Four heaters (Ah Ah Ah!):

    assert("Add Fourth Heater", heaterManagerAddHeater(&(testHeaters[3])));

    //Make sure all heaters are off
    disableAll(&testHeaters[0]);
    disableAll(&testHeaters[1]);
    disableAll(&testHeaters[2]);
    disableAll(&testHeaters[3]);

    testHeaters[0].activeRequest = true;
    testHeaters[1].activeRequest = true;
    testHeaters[2].activeRequest = true;
    testHeaters[3].activeRequest = true;

    //Run the heater management logic
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], true);
    checkHeaterEnableStatus(&testHeaters[1], true);
    checkHeaterEnableStatus(&testHeaters[2], false);
    checkHeaterEnableStatus(&testHeaters[3], false);

    //Run the management logic again to switch heaters
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], true);
    checkHeaterEnableStatus(&testHeaters[2], true);
    checkHeaterEnableStatus(&testHeaters[3], false);

    //Run the management logic again to switch heaters
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], false);
    checkHeaterEnableStatus(&testHeaters[2], true);
    checkHeaterEnableStatus(&testHeaters[3], true);

    //Run the management logic again to switch heaters
    manageActiveHeaters();

    //Expect the heaters to be active:
    checkHeaterEnableStatus(&testHeaters[0], true);
    checkHeaterEnableStatus(&testHeaters[1], false);
    checkHeaterEnableStatus(&testHeaters[2], false);
    checkHeaterEnableStatus(&testHeaters[3], true);

    //Disable the heater's active request
    testHeaters[0].activeRequest = false;
    testHeaters[1].activeRequest = false;
    testHeaters[2].activeRequest = false;
    testHeaters[3].activeRequest = false;

    //Run the disable inactive heaters call
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[0]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[1]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[2]));
    assert("Heater Pair Disable", manageDisabledHeaters(&testHeaters[3]));

    //Expect all heaters to be off:
    checkHeaterEnableStatus(&testHeaters[0], false);
    checkHeaterEnableStatus(&testHeaters[1], false);
    checkHeaterEnableStatus(&testHeaters[2], false);
    checkHeaterEnableStatus(&testHeaters[3], false);
}

void checkHeaterEnableStatus(heaterPair_t* pHeaterPair, bool expected)
{
	assert("Heater Pair Check", expected == (pHeaterPair->activeEnable));

    assert("Main Heater Check", expected == (pHeaterPair->mainHeater.enableStatus));
    assert("Wrap Heater Check", expected == (pHeaterPair->wrapHeater.enableStatus));
}

void disableAll(heaterPair_t* pHeater)
{
    //Heater Pair
    pHeater->activeEnable = false;
    pHeater->activeRequest = false;

    //Main heater
    pHeater->mainHeater.enableStatus = false;

    //Wrap heater
    pHeater->wrapHeater.enableStatus = false;
}

