/*
 * ADT7410_test.c
 *
 *  Created on: Aug 26, 2015
 *      Author: Stephen
 */

#include "ADT7410_test.h"

bool adt7410test()
{
	bool status, testStatus = true;
	ADT7410_DATA readVal, writeVal;
	uint16_t tempData;

	//Initialize I2C peripheral
	status = i2cInit();
	if(false == status)
	{
		testStatus = false;
	}

	//Test initial status register value
	status = adt7410ReadRegister(ADT7410_STATUS_REG, &readVal);
	if((0x80 != readVal) || (false == status))
	{
		testStatus = false;
	}

	readVal = 0x00;

	//Test initial configuration register value
	status = adt7410ReadRegister(ADT7410_CONFIG_REG, &readVal);
	if((0x00 != readVal) || (false == status))
	{
		testStatus = false;
	}

	readVal = 0x00;

	//Write configuration register and test value
	writeVal = 0x83;
	status = adt7410WriteRegister(ADT7410_CONFIG_REG, &writeVal);
	if(true == status)
	{
		status = adt7410ReadRegister(ADT7410_CONFIG_REG, &readVal);
		if((0xC3 != readVal) || (false == status))
		{
			testStatus = false;
		}

		readVal = 0x00;
	}
	else
	{
		testStatus = false;
	}

	while(1)
	{
	adt7410ReadRegister(ADT7410_TEMP_MSB_REG, &readVal);
	tempData = readVal;
	tempData = (tempData<<8) & 0xFF00;

	adt7410ReadRegister(ADT7410_TEMP_LSB_REG, &readVal);
	tempData |= (uint16_t)(readVal & 0x00FF);
	}

	return testStatus;
}
