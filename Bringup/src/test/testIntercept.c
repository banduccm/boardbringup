/*
 * testIntercept.c
 *
 *  Created on: Aug 5, 2015
 *      Author: Mike
 */

#ifdef UNIT_TEST

//DO NOT include testIntercept.h
#include "gpio.h"

bool gpioMockWrite(gpio_t* pGpio, gpioState_t state)
{
    return true;
}

bool gpioMockRead(gpio_t* pGpio, gpioState_t* state)
{
    return true;
}

#endif
