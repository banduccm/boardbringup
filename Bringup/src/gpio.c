/*
 * gpio.c
 *
 *  Created on: May 5, 2015
 *      Author: Mike
 */

//Include these for the generic IO read/write functions
#include "r_pdl_io_port.h"
#include "r_pdl_definitions.h"

#include "gpio.h"

/************************************************************
 * gpioInit:                                                *
 *  Calls the PORT_SET routine to configure each of the     *
 *  ports configured via the RPDL.                          *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool gpioInit(void)
{
    bool retVal = true;

    if(!initFlags.gpio)
    {   //Only initialize if uninitialized
        retVal = R_PG_IO_PORT_Set_P0();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P1();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P2();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P4();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P5();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P6();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_P8();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_PA();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_PB();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_PC();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_PD();

        if(retVal)
            retVal = R_PG_IO_PORT_Set_PE();
    }

    if(retVal)
    {   //If initialization is successful, set the init flag to true
        initFlags.gpio = 1;
    }
    return retVal;
}

/************************************************************
 * gpioWrite:                                               *
 *  Ensures the state data (high/low) is set correctly      *
 *  according to the pin's polarity, then calls the RPDL    *
 *  Port Write routine to set the output.                   *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool gpioWrite(gpio_t* pGpio, gpioState_t state)
{
    bool retVal = true;
    uint8_t data = 0x00;

    //Assume active low
    if(state != eGpioStateActive)
        data = 0xFF;

    //If the polarity is active high, flip the data bit
    if(pGpio->polarity == eGpioPolarityActiveHigh)
    {
        data = !data;
    }

    //Write the data to the port
    retVal = R_IO_PORT_Write(pGpio->port, data);

    return retVal;
}

/************************************************************
 * gpioRead:                                                *
 *  Reads the data from the given pin, then converts it to  *
 *  a GPIO State type based on the pin's polarity.          *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool gpioRead(gpio_t* pGpio, gpioState_t* pState)
{
    bool retVal = true;
    uint8_t data = 0x00;

    retVal = R_IO_PORT_Read(pGpio->port, &data);

    if(retVal)
    {
        if(pGpio->polarity == eGpioPolarityActiveHigh)
        {
            *pState = (gpioState_t)(data & 0x01);
        }
        else if(pGpio->polarity == eGpioPolarityActiveLow)
        {
            data = !(data & 0x01);
            *pState = (gpioState_t)(data & 0x01);
        }
        else
        {
            retVal = false;
        }
    }

    return retVal;
}
