/*
 * ADT7410.c
 *
 *  Created on: Aug 5, 2015
 *      Author: Stephen
 */

#include "ADT7410.h"

/*! \fn bool adt7410ReadRegister(ADT7410_REG addr, ADT7410_DATA* data)
 *  \brief Performs a read of the given register.
 *  \param [in] addr - Address of the register to be read
 *  \param [out] data - Data read from the register
 *  \return success. False if the I2C transfers fail.
 */
bool adt7410ReadRegister(ADT7410_REG addr, ADT7410_DATA* data)
{
	bool status;

	//send the register to be read
	status = i2cTransmitWithoutStop(eI2cDeviceTemperature, &addr, 1);

	//Read the register if the initial transfer succeeds
	if(status)
	{
		status = i2cRestartReceive(eI2cDeviceTemperature, data, 1);
	}

	return status;
}

/*! \fn bool adt7410WriteRegister(ADT7410_REG addr, ADT7410_DATA* data)
 *  \brief Performs a write of the given register.
 *  \param [in] addr - Address of the register to be written
 *  \param [in] data - Data to be written to the register
 *  \return success. False if the I2C transfers fail.
 */
bool adt7410WriteRegister(ADT7410_REG addr, ADT7410_DATA* data)
{
	bool status;
	ADT7410_DATA i2cData[2];

	i2cData[0] = addr;
	i2cData[1] = *data;

	status = i2cTransmit(eI2cDeviceTemperature, i2cData, 2);

	return status;
}

/*! \fn bool adt7410ConvertTempToRegisterValue(double temperature, uint16_t* retVal)
 *  \brief Converts a temperature value stored as a double to a 13- or 16-bit
 *         value for transfer to the ADT7410.
 *  \param [in] temperature - The temperature to be converted
 *  \param [out] regVal - The converted temperature
 *  \return success. False if the input temperature is out of bounds or if the ADT7410
 *           settings are not properly configured.
 */
bool adt7410ConvertTempToRegisterValue(double temperature, uint16_t* regVal)
{
	bool status = true;

	//Check temperature bounds
	if((temperature > MAX_TEMP_SET) && (temperature > MIN_TEMP_SET))
	{
		status = false;
	}
	else
	{
		//Convert integer temperature to value to write to ADT7410 register
		if(NUM_TEMP_BITS == TEMP_16_BITS)
		{
			*regVal = temperature*128;
		}
		else if(NUM_TEMP_BITS == TEMP_13_BITS)
		{
			*regVal = temperature*16;
		}
		else
		{
			status = false;
		}
	}

	return status;
}

/*! \fn bool adt7410ConvertRegisterValueToDouble(uint16_t regVal, double* temp)
 *  \brief Converts a temperature value retreived from the ADT7410 to a double.
 *  \param [in] regval - The temperature to be converted
 *  \param [out] temp - The converted temperature
 *  \return success. False if the ADT7410 settings are not properly configured.
 */
bool adt7410ConvertRegisterValueToDouble(uint16_t regVal, double* temp)
{
	bool status = true;

	if(NUM_TEMP_BITS == TEMP_16_BITS)
	{
		*temp = (double)(regVal)/128.0;
	}
	else if(NUM_TEMP_BITS == TEMP_13_BITS)
	{
		*temp = (double)(regVal)/16.0;
	}
	else
	{
		status = false;
	}

	return status;
}

/*! \fn bool adt7410GetTemperature(double* temp)
 *  \brief Performs a read of the temperature from the ADT7410 and
 *         converts the value to a double.
 *  \param [out] temp - The read and converted temperature
 *  \return success. False if the ADT7410 settings are not properly
 *            configured or the I2C transfers fail.
 */
bool adt7410GetTemperature(double* temp)
{
	bool status = true;

	ADT7410_DATA readValMSB, readValLSB;
	uint16_t tempData;

	status = adt7410ReadRegister(ADT7410_TEMP_MSB_REG, &readValMSB);

	if(true == status)
	{
		status = adt7410ReadRegister(ADT7410_TEMP_LSB_REG, &readValLSB);
	}

	if(true == status)
	{
		tempData = (readValMSB << 8) + readValLSB;
		status = adt7410ConvertRegisterValueToDouble(tempData, temp);
	}

	return status;
}

/*! \fn bool adt7410GetStatus(ADT7410_DATA* data)
 *  \brief Performs a read of the ADT7410 status register.
 *  \param [out] data - Pointer to the status register value.
 *  \return success. False if the read register routine fails.
 */
bool adt7410GetStatus(ADT7410_DATA* pData)
{
	bool status;

	status = adt7410ReadRegister(ADT7410_STATUS_REG, pData);

	return status;
}

/*! \fn bool adt7410GetConfig(ADT7410_DATA* data)
 *  \brief Performs a read of the ADT7410 configuration register.
 *  \param [out] data - Pointer to the configuration register value.
 *  \return success. False if the read register routine fails.
 */
bool adt7410GetConfig(ADT7410_DATA* pData)
{
	bool status;

	status = adt7410ReadRegister(ADT7410_CONFIG_REG, pData);

	return status;
}

/*! \fn bool adt7410SetConfig(ADT7410_DATA* data)
 *  \brief Performs a write of the ADT7410 configuration register.
 *  \param [in] data - Pointer to the configuration register value.
 *  \return success. False if the read register routine fails.
 */
bool adt7410SetConfig(ADT7410_DATA* pData)
{
	bool status;

	status = adt7410WriteRegister(ADT7410_CONFIG_REG, pData);

	return status;
}

/*! \fn bool adt7410SetHighTemp(double temperature)
 *  \brief Sets the High Temp register value on the ADT7410
 *  \param [in] temperature - The high temp value to be written
 *  \return success. False if the given temperature is out of bounds
 *             or if the I2C transfers fail.
 */
bool adt7410SetHighTemp(double temperature)
{
	bool status;
	ADT7410_DATA regSet;
	uint16_t setVal;

	//Convert temperature to register settings
	status = adt7410ConvertTempToRegisterValue(temperature, &setVal);

	//Set LSB if temperature is within bounds
	if(true == status)
	{
		regSet = (char)(setVal & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_HIGH_SET_LSB_REG, &regSet);
	}

	//Set MSB if LSB succeeds
	if(true == status)
	{
		regSet = (char)((setVal >> 8) & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_HIGH_SET_MSB_REG, &regSet);
	}

	return status;
}

/*! \fn bool adt7410SetLowTemp(double temperature)
 *  \brief Sets the Low Temp register value on the ADT7410
 *  \param [in] temperature - The low temp value to be written
 *  \return success. False if the given temperature is out of bounds
 *             or if the I2C transfers fail.
 */
bool adt7410SetLowTemp(double temperature)
{
	bool status;
	ADT7410_DATA regSet;
	uint16_t setVal;

	//Convert temperature to register settings
	status = adt7410ConvertTempToRegisterValue(temperature, &setVal);

	//Set LSB if temperature is within bounds
	if(true == status)
	{
		regSet = (char)(setVal & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_LOW_SET_LSB_REG, &regSet);
	}

	//Set MSB if LSB succeeds
	if(true == status)
	{
		regSet = (char)((setVal >> 8) & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_LOW_SET_MSB_REG, &regSet);
	}

	return status;
}

/*! \fn bool adt7410SetCritTemp(double temperature)
 *  \brief Sets the Critical Temp register value on the ADT7410
 *  \param [in] temperature - The crit temp value to be written
 *  \return success. False if the given temperature is out of bounds
 *             or if the I2C transfers fail.
 */
bool adt7410SetCritTemp(double temperature)
{
	bool status;
	ADT7410_DATA regSet;
	uint16_t setVal;

	//Convert temperature to register settings
	status = adt7410ConvertTempToRegisterValue(temperature, &setVal);

	//Set LSB if temperature is within bounds
	if(true == status)
	{
		regSet = (char)(setVal & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_CRIT_SET_LSB_REG, &regSet);
	}

	//Set MSB if LSB succeeds
	if(true == status)
	{
		regSet = (char)((setVal >> 8) & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_CRIT_SET_MSB_REG, &regSet);
	}

	return status;
}

/*! \fn bool adt7410SetHysteresis(int hysteresis)
 *  \brief Sets the Hysteresis register value on the ADT7410
 *  \param [in] hysteresis - The hysteresis value to be written
 *  \return success. False if the given hysteresis value is out
 *             of bounds or if the I2C transfers fail.
 */
bool adt7410SetHysteresis(int hysteresis)
{
	bool status;
	ADT7410_DATA regSet;

	//Check that value is not above max hysteresis value for ADT7410
	if((hysteresis > 15) || (hysteresis < 0))
	{
		status =  false;
	}

	//If temperature is within bounds, set it to hysteresis register
	if(true == status)
	{
		regSet = (uint8_t)(hysteresis & 0x00FF);
		status = adt7410WriteRegister(ADT7410_T_HYST_SET_REG, &regSet);
	}

	return status;
}

/*! \fn bool adt7410GetID(ADT7410_DATA* id)
 *  \brief Gets the ID of the ADT7410
 *  \param [out] id - The ID read from the ADT7410
 *  \return success. False if the I2C transfers fail.
 */
bool adt7410GetID(ADT7410_DATA* id)
{
	bool status;

	status = adt7410ReadRegister(ADT7410_ID_REG, id);

	return status;
}

/*! \fn bool adt7410swReset(void)
 *  \brief Performs a software reset of the ADT7410
 *  \return success. False if the I2C transfers fail.
 */
bool adt7410swReset(void)
{
	bool status;
	uint8_t data = ADT7410_SW_RESET_REG;

	status = i2cTransmit(eI2cDeviceTemperature, &data, 1);

	return status;
}
