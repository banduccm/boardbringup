/*
 * pump.c
 *
 *  Created on: Jun 14, 2015
 *      Author: Mike
 */

#include "pump.h"

/*! \fn bool pumpInit(void)
 *  \brief Ensures all hardware interfaces used by the Heater class is initialized
 *  \return success. False if any of the child interfaces return an init error.
 */
bool pumpInit(void)
{
    bool success = false;

    success = gpioInit();

    if(success)
    {
        success = adcInit();
    }

    if(success)
    {
        success = adcStart();
    }

    return success;
}

/*! \fn bool pumpEnable(pump_t* pPump, bool enable)
 *  \brief Sets the enable pin for the given pump according to the given state
 *  \param pPump - Pointer to the pump to enable or disable
 *  \param enable - Flag to determine if the pump should be enabled or disabled
 *  \return success. False if the GPIO write returns an error
 */
bool pumpEnable(pump_t* pPump, bool enable)
{
    bool success = true;

    if(enable)
    {
        success = gpioWrite(&(pPump->enable), eGpioStateActive);
    }
    else
    {
        success = gpioWrite(&(pPump->enable), eGpioStateInactive);
    }

    return success;
}
