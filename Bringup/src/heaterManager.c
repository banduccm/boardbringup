/*
 * heaterManager.c
 *
 *  Created on: Jul 27, 2015
 *      Author: Mike
 */

#include "heaterManager.h"
#include "testIntercept.h"

//Private (file scope) Variables
//Create an empty array to store up to the max number of heaters
static queue_t m_qHeaters;
static queue_t m_qActiveHeaters;
static queue_t m_qWaitingHeaters;
static bool queuesInitted = false;

bool heaterManagerInit(void)
{
	bool success = true;

	//Make sure we don't blow away the queues if this gets Initted more than once
	if(true != queuesInitted)
	{
		success = queueInit(&m_qActiveHeaters, MAX_ACTIVE_HEATERS);

		if(success)
		{
			success = queueInit(&m_qHeaters, NUM_CONSERVERS);
		}

		if(success)
		{
			success = queueInit(&m_qWaitingHeaters, NUM_CONSERVERS);
		}

		if(success)
		{
			queuesInitted = true;
		}
	}

	return success;
}

/*! \fn bool heaterManagerAddHeater(heater_t* pHeater)
 *  \brief This routine will add a heater to the manager's array
 *  \param pHeater - pointer to the heater object to add to the manager
 *  \return success. False if a null heater is added or if the array is full
 */
bool heaterManagerAddHeater(heaterPair_t* pHeaterPair)
{
    bool success = true;

    //Make sure the heater is valid and the array has space
    if(NULL != pHeaterPair)
    {	//Make sure this heater hasn't already been added
    	if(false == queueFind(&m_qHeaters, (uint32_t)pHeaterPair))
    	{
			success = enqueue(&m_qHeaters, (uint32_t)pHeaterPair);
    	}
    }

    return success;
}

/*! \fn bool manageActiveHeaters(void)
 *  \brief A function to control which heaters are currently active to avoid overdrawing power
 *
 *  This routine will manage the active heaters in a round-robin fashion. It will iterate
 *  through the heater array and enable them at time in slices of time determined by the
 *  caller. This routine should be called periodically at a roughly predictable rate.
 *
 *  \return success. False if one of the heaterControl calls fails
 */
bool manageActiveHeaters(void)
{
    bool success = false;
    uint16_t i = 0;
    heaterPair_t* pTempHeaterPair = NULL;

    for(i = 0; i < m_qHeaters.filledElements; i++)
    {
    	success = dequeue(&m_qHeaters, (uint32_t*)(&pTempHeaterPair));

    	if(true == pTempHeaterPair->activeRequest)
    	{
    		if(false == queueFind(&m_qActiveHeaters, (uint32_t)pTempHeaterPair))
    		{
    			if(false == queueFind(&m_qWaitingHeaters, (uint32_t)pTempHeaterPair))
    			{
    				enqueue(&m_qWaitingHeaters, (uint32_t)pTempHeaterPair);
    			}
    		}
    	}

    	enqueue(&m_qHeaters, (uint32_t)pTempHeaterPair);
    }

    if(m_qHeaters.filledElements > MAX_ACTIVE_HEATERS)
    {
    	do
    	{
			//If we already have the max number of active heaters...
			if(MAX_ACTIVE_HEATERS <= m_qActiveHeaters.filledElements)
			{
				//...Turn off the oldest item in the list
				success = dequeue(&m_qActiveHeaters, (uint32_t*)(&pTempHeaterPair));
				if(success)
				{
					success &= heaterPairControl(pTempHeaterPair, false);
				}

				//If the heater still wants to be enabled, re-enqueue it at the end of the waiting queue
				if((true == success))// && (true == pTempHeaterPair->activeRequest))		//@TODO: Figure out what the ActiveRequest vs ActiveEnabled logic should do here (if anything)
				{
					success = enqueue(&m_qWaitingHeaters, (uint32_t)pTempHeaterPair);
				}
			}

			//If there is a new heater to enable...
			if(0 < m_qWaitingHeaters.filledElements)
			{
				//Enable the new heater
				success = dequeue(&m_qWaitingHeaters, (uint32_t*)(&pTempHeaterPair));
				success &= heaterPairControl(pTempHeaterPair, true);
				success = enqueue(&m_qActiveHeaters, (uint32_t)pTempHeaterPair);
			}
    	} while((MAX_ACTIVE_HEATERS > m_qActiveHeaters.filledElements) && (MAX_ACTIVE_HEATERS <= (m_qActiveHeaters.filledElements + m_qWaitingHeaters.filledElements)));	//@todo: This should: loop only if we have 2 (or more) heaters that want to be active *AND* fewer than 2 heaters are enabled
    }
    else if(m_qHeaters.filledElements > 0)
    {   //If the number of heaters that should be on is less than the max
        while( 0 != m_qWaitingHeaters.filledElements)
        {
        	success &= dequeue(&m_qWaitingHeaters, (uint32_t*)(&pTempHeaterPair));
        	if(true == pTempHeaterPair->activeRequest)
        	{
        		success &= heaterPairControl(pTempHeaterPair, true);
        	}
        	success &= enqueue(&m_qActiveHeaters, (uint32_t)pTempHeaterPair);
        }
    }

    return success;
}

bool manageDisabledHeaters(heaterPair_t* pHeaterPair)
{
    bool success = false;
    heaterPair_t* tempHeaterPair;
    uint16_t i = 0;

    //If a heater should be OFF, turn it OFF. (safety first!)
    if(false == pHeaterPair->activeRequest)
    {
        success = heaterPairControl(pHeaterPair, false);
    }

    for(i = m_qActiveHeaters.filledElements; i > 0; i--)
    {
    	success &= dequeue(&m_qActiveHeaters, (uint32_t*)(&tempHeaterPair));
    	if(tempHeaterPair != pHeaterPair)
    	{	//Find the heater we want to disable
    		success &= enqueue(&m_qActiveHeaters, (uint32_t)tempHeaterPair);
    	}
    }

    for(i = m_qWaitingHeaters.filledElements; i > 0; i--)
    {
    	success &= dequeue(&m_qWaitingHeaters, (uint32_t*)(&tempHeaterPair));
    	if(tempHeaterPair != pHeaterPair)
    	{
    		success &= enqueue(&m_qWaitingHeaters, (uint32_t)tempHeaterPair);
    	}
    }

    return success;
}
