/*
 * controlTasks.c
 *
 *  Created on: Jul 19, 2015
 *      Author: Mike
 */

#include "controlTasks.h"

bool heaterTask(conserver_t* pConserver);
bool pumpTask(pumpController_t* pPumpController);
bool conserverCleanup(conserver_t* pConserver);

/*! \fn bool conserverTask(conserver_t* pConserver)
 *  \brief Performs a management cycle for a given conserver: Updates the switch
 *         states, manages the conserver's heaters and pumps, and handles the conserver's detect state.
 *  \param pConserver - the conserver to manage
 *  \return success. False if the conserver is invalid or any of the task functions report failure
 */
bool conserverTask(conserver_t* pConserver)
{
    bool success = true;

    if(NULL != pConserver)
    {
        //Ask the conserver to update all its status flags
        success = conserverReadState(pConserver);

        if(success && (true == pConserver->detectStatus))
        {
            success = heaterTask(pConserver);

            if(success)
            {
                success = pumpTask(NULL);
            }
        }
        else if (false == pConserver->detectStatus)
        {
            success = conserverCleanup(pConserver);
        }
    }
    else
    {
        success = false;
    }

    return success;
}

/*! \fn bool heaterTask(conserver_t* pConserver)
 *  \brief Performs a management cycle for a given converver's heaters. Handles all of the error switches
 *         for a heater and manages any inactive heaters.
 *  \param pConserver - The conserver to manage
 *  \return success. False if any of the task functions fail
 */
bool heaterTask(conserver_t* pConserver)
{
    bool success = true;

    //Handle error cases:
    if((true == pConserver->overTempStatus) || (true == pConserver->lowPressureStatus))
    {
        success &= heaterPairRequest(&(pConserver->heaters), false);
    }
    else
    {
        if(false == pConserver->highTempStatus)
        {
            success &= heaterPairRequest(&(pConserver->heaters), true);
        }
        else
        {
            success &= heaterPairRequest(&(pConserver->heaters), false);
        }
    }

    success &= conserverDisableInactiveHeaters(pConserver);



    return success;
}

/*! \fn bool pumpTask(pumpController_t* pPumpController)
 *  \brief Performs a management cycle for a given pump controller. Handles all of the error switches
 *         and manages pump activation
 *  \param pPumpController - The pump controller to manage
 *  \return success. False if any of the task functions fail
 */
bool pumpTask(pumpController_t* pPumpController)
{
    bool success = true;

    //If the High Pressure or Low Level switches are Active
    if((true == pPumpController->lowLevelFloatStatus) || (true == pPumpController->inletHighPressureStatus))
    {   //Turn off Inlet and Return Pumps
        success &= pumpEnable(&(pPumpController->inletPump), false);
        success &= pumpEnable(&(pPumpController->returnPump), false);
    }
    else
    {   //The return pump should be enabled during normal operation
        success &= pumpEnable(&(pPumpController->returnPump), true);

        if(false == pPumpController->highLevelFloatStatus)
        {
            success &= pumpEnable(&(pPumpController->inletPump), true);
        }
        else
        {
            success &= pumpEnable(&(pPumpController->inletPump), false);
        }
    }

    return success;
}

/*! \fn bool conserverCleanup(conserver_t* pConserver)
 *  \brief Handles disabling the heaters for a conserver that has been disconnected
 *  \param pConserver - The conserver to manage
 *  \return success. False if any of the task functions fail
 */
bool conserverCleanup(conserver_t* pConserver)
{
    bool success = true;

    if(true == pConserver->heaters.mainHeater.enableStatus)
    {
        success &= heaterControl(&(pConserver->heaters.mainHeater), false);
    }

    if(true == pConserver->heaters.wrapHeater.enableStatus)
    {
        success &= heaterControl(&(pConserver->heaters.wrapHeater), false);
    }

    return success;
}

void ControlTaskTimerInterrupt()
{
}

void RTCOneSecTimerInterrupt()
{
}
