/*
 * spi.c
 *
 *  Created on: May 15, 2015
 *      Author: Mike
 */

#include "iodefine_RPDL.h"
#include "spi.h"
#include "r_pdl_spi.h"
#include "r_pdl_definitions.h"

bool spiInit(void)
{
    bool retVal = true;

    if(!initFlags.spi)
    {
        retVal = R_PG_RSPI_Set_C1();
        if(retVal)
        {
            retVal = R_PG_RSPI_SetCommand_C1();
        }
    }

    if(retVal)
    {
        initFlags.spi = 1;
    }

    return retVal;
}

bool spiTransfer(spiDevice_t device, uint32_t* txData, uint32_t* rxData, uint32_t count)
{
    bool retVal = false;
    uint32_t slaveSelect = 0;
    uint32_t i = 0;

    if(device < eSpiDeviceInvalid)
    {
    	if(eSpiDeviceWiFi == device)
    	{
    		slaveSelect = PDL_SPI_ASSERT_SSL0;
    	}
    	else if(eSpiDeviceFlash == device)
    	{
    		slaveSelect = PDL_SPI_ASSERT_SSL1;
    	}
        //Set the correct Chip Select pin
    	retVal = R_SPI_Command(1,0,PDL_SPI_CLOCK_MODE_0 | PDL_SPI_DIV_1 | slaveSelect | PDL_SPI_SSL_NEGATE | PDL_SPI_LENGTH_8 | PDL_SPI_MSB_FIRST, PDL_SPI_CLOCK_DELAY_MINIMUM | PDL_SPI_SSL_DELAY_MINIMUM | PDL_SPI_NEXT_DELAY_MINIMUM);

    	for(i = 0; i < count; i++)
    	{
    		retVal = R_PG_RSPI_TransferAllData_C1(&txData[i], &rxData[i], 1);
    	}
    }

    return retVal;
}

void Spi1IntFunc(void)
{

}
