/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_SCI_C6.c
* Version      : 1.00
* Device(s)    : 
* Tool-Chain   : 
* H/W Platform : 
* Description  : 
* Limitations  : 
******************************************************************************
* History : 21.07.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_pdl_sci.h"
#include "r_pdl_definitions.h"
#include "R_PG_IntFuncsExtern.h"


/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_Set_C6(void)
*
* Function Name: R_PG_SCI_Set_C6
*
* Description  : Set up the serial I/O
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_Create
*                   : R_SCI_Set
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_Set_C6(void)
{
	bool res;

	res = R_SCI_Set(
		6,
		PDL_SCI_PIN_SCI6_SSCL6_P33 | PDL_SCI_PIN_SCI6_SSDA6_P32
	);

	if( !res ){
		return res;
	}

	return R_SCI_Create(
		6,
		PDL_SCI_SYNC | PDL_SCI_IIC_MODE | PDL_SCI_IIC_FILTER_DISABLED | PDL_SCI_IIC_DELAY_SDA_0_1,
		//BIT_31 | PDL_SCI_PCLK_DIV_16 | 92 | (1008 & 0x00FFFF00ul),
		100000,
		0,
		0
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_Send_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
*
* Function Name: R_PG_SCI_I2CMode_Send_C6
*
* Description  : Send data in the simple I2C mode
*
* Arguments    : bool addr_10bit : Target slave address format (1:10 bits  0:7 bits).
*              : uint16_t slave : Target slave address.
*              : uint8_t * data : The start address of the data to be sent.
*              : uint16_t count : The number of the data to be sent.
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_IIC_Write
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_Send_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
{
	uint16_t addr_size = PDL_SCI_IIC_7_BIT_SLAVE_ADDRESS;

	if( addr_10bit )
	{
		addr_size = PDL_SCI_IIC_10_BIT_SLAVE_ADDRESS;
	}
	
	if( data == 0 ){ return false; }

	return R_SCI_IIC_Write(
		6,
		PDL_SCI_IIC_DMAC_DTC_TRIGGER_DISABLE | addr_size,
		slave,
		count,
		data,
		PDL_NO_FUNC
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_SendWithoutStop_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
*
* Function Name: R_PG_SCI_I2CMode_SendWithoutStop_C6
*
* Description  : Send data in the simple I2C mode ( No stop condition )
*
* Arguments    : bool addr_10bit : Target slave address format (1:10 bits  0:7 bits).
*              : uint16_t slave : Target slave address.
*              : uint8_t * data : The start address of the data to be sent.
*              : uint16_t count : The number of the data to be sent.
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_IIC_Write
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_SendWithoutStop_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
{
	uint16_t addr_size = PDL_SCI_IIC_7_BIT_SLAVE_ADDRESS;

	if( addr_10bit )
	{
		addr_size = PDL_SCI_IIC_10_BIT_SLAVE_ADDRESS;
	}
	
	if( data == 0 ){ return false; }

	return R_SCI_IIC_Write(
		6,
		PDL_SCI_IIC_7_BIT_SLAVE_ADDRESS | PDL_SCI_IIC_NOSTOP,
		slave,
		count,
		data,
		PDL_NO_FUNC
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_GenerateStopCondition_C6(void)
*
* Function Name: R_PG_SCI_I2CMode_GenerateStopCondition_C6
*
* Description  : Generate a stop condition
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_GenerateStopCondition_C6(void)
{
	return R_SCI_Control(
		6,
		PDL_SCI_IIC_STOP
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_GetSentDataCount_C6(uint16_t * count)
*
* Function Name: R_PG_SCI_GetSentDataCount_C6
*
* Description  : Acquires the count of transmitted serial data
*
* Arguments    : uint16_t * count : The storage location for the number of bytes
*              :                  : that have been transmitted in the current transmission.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_SCI_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_GetSentDataCount_C6(uint16_t * count)
{
	uint8_t status;

	if( count == 0 ){ return false; }

	return R_SCI_GetStatus(
		6,
		&status,
		PDL_NO_PTR,
		count,
		PDL_NO_PTR
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_Receive_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
*
* Function Name: R_PG_SCI_I2CMode_Receive_C6
*
* Description  : Receive data in the simple I2C mode
*
* Arguments    : bool addr_10bit : Target slave address format (1:10 bits  0:7 bits).
*              : uint16_t slave : Target slave address.
*              : uint8_t * data : The start address of the storage area for the expected data.
*              : uint16_t count : The number of the data to be received.
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_IIC_Read
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_Receive_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
{
	uint16_t addr_size = PDL_SCI_IIC_7_BIT_SLAVE_ADDRESS;

	if( addr_10bit )
	{
		addr_size = PDL_SCI_IIC_10_BIT_SLAVE_ADDRESS;
	}
	
	if( data == 0 ){ return false; }

	return R_SCI_IIC_Read(
		6,
		PDL_SCI_IIC_DMAC_DTC_TRIGGER_DISABLE | addr_size,
		slave,
		count,
		data,
		PDL_NO_FUNC
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_RestartReceive_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
*
* Function Name: R_PG_SCI_I2CMode_RestartReceive_C6
*
* Description  : Receive data in the simple I2C mode ( Re-start condition )
*
* Arguments    : bool addr_10bit : Target slave address format (1:10 bits  0:7 bits).
*              : uint16_t slave : Target slave address.
*              : uint8_t * data : The start address of the storage area for the expected data.
*              : uint16_t count : The number of the data to be received.
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_SCI_IIC_Read
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_RestartReceive_C6(bool addr_10bit, uint16_t slave, uint8_t * data, uint16_t count)
{
	uint16_t addr_size = PDL_SCI_IIC_7_BIT_SLAVE_ADDRESS;

	if( addr_10bit )
	{
		addr_size = PDL_SCI_IIC_10_BIT_SLAVE_ADDRESS;
	}
	
	if( data == 0 ){ return false; }

	return R_SCI_IIC_Read(
		6,
		PDL_SCI_IIC_DMAC_DTC_TRIGGER_DISABLE | addr_size | PDL_SCI_IIC_RESTART,
		slave,
		count,
		data,
		PDL_NO_FUNC
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_GetReceivedDataCount_C6(uint16_t * count)
*
* Function Name: R_PG_SCI_GetReceivedDataCount_C6
*
* Description  : Acquires the count of received serial data
*
* Arguments    : uint16_t * count : The storage location for the number of bytes
*              :                  : that have been received in the current reception process.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_SCI_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_GetReceivedDataCount_C6(uint16_t * count)
{
	uint8_t status;

	if( count == 0 ){ return false; }

	return R_SCI_GetStatus(
		6,
		&status,
		PDL_NO_PTR,
		PDL_NO_PTR,
		count
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_I2CMode_GetEvent_C6(bool * nack)
*
* Function Name: R_PG_SCI_I2CMode_GetEvent_C6
*
* Description  : Get the detected event in the simple I2C mode
*
* Arguments    : bool * nack : The address of the storage area for a NACK detection flag.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_SCI_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_I2CMode_GetEvent_C6(bool * nack)
{
	uint8_t status;
	bool res;

	res = R_SCI_GetStatus(
		6,
		&status,
		PDL_NO_PTR,
		PDL_NO_PTR,
		PDL_NO_PTR
	);

	if( nack ){ *nack = status & 0x01; }

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_SCI_StopModule_C6(void)
*
* Function Name: R_PG_SCI_StopModule_C6
*
* Description  : Shut down the serial I/O channel
*
* Arguments    : None
*
* Return Value : true  : Shutting down succeeded.
*              : false : Shutting down failed.
*
* Calling Functions : R_SCI_Destroy
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_SCI_StopModule_C6(void)
{
	return R_SCI_Destroy( 6 );

}



