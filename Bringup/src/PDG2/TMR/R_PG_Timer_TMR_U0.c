/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_Timer_TMR_U0.c
* Version      : 1.00
* Device(s)    : 
* Tool-Chain   : 
* H/W Platform : 
* Description  : 
* Limitations  : 
******************************************************************************
* History : 21.07.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_pdl_tmr.h"
#include "r_pdl_definitions.h"
#include "R_PG_IntFuncsExtern.h"


/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_Start_TMR_U0(void)
*
* Function Name: R_PG_Timer_Start_TMR_U0
*
* Description  : Set up the TMR and start the count
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_TMR_CreateUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_Start_TMR_U0(void)
{
	return R_TMR_CreateUnit(
		0,
		PDL_TMR_CLK_PCLK_DIV_64 | PDL_TMR_CLEAR_CM_A | PDL_TMR_ADC_TRIGGER_ENABLE,
		PDL_TMR_OUTPUT_IGNORE_CM_A | PDL_TMR_OUTPUT_IGNORE_CM_B,
		0,
		1199,
		599,
		PDL_NO_FUNC,
		PDL_NO_FUNC,
		PDL_NO_FUNC,
		0
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_HaltCount_TMR_U0(void)
*
* Function Name: R_PG_Timer_HaltCount_TMR_U0
*
* Description  : Halt the TMR count
*
* Arguments    : None
*
* Return Value : true  : Halting succeeded.
*              : false : Halting failed.
*
* Calling Functions : R_TMR_ControlUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_HaltCount_TMR_U0(void)
{
	return R_TMR_ControlUnit(
		0,
		PDL_TMR_STOP,
		0,
		0,
		0
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_ResumeCount_TMR_U0(void)
*
* Function Name: R_PG_Timer_ResumeCount_TMR_U0
*
* Description  : Resume the TMR count
*
* Arguments    : None
*
* Return Value : true  : Resuming count succeeded.
*              : false : Resuming count failed.
*
* Calling Functions : R_TMR_ControlUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_ResumeCount_TMR_U0(void)
{
	return R_TMR_ControlUnit(
		0,
		PDL_TMR_START,
		0,
		0,
		0
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_GetCounterValue_TMR_U0(uint16_t * counter_val)
*
* Function Name: R_PG_Timer_GetCounterValue_TMR_U0
*
* Description  : Acquire the TMR counter value
*
* Arguments    : uint16_t * counter_val : Destination for storage of the counter value.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_TMR_ReadUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_GetCounterValue_TMR_U0(uint16_t * counter_val)
{
	if( counter_val == 0 ){ return false; }

	return R_TMR_ReadUnit(
		0,
		PDL_NO_PTR,
		counter_val,
		PDL_NO_PTR,
		PDL_NO_PTR
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_SetCounterValue_TMR_U0(uint16_t counter_val)
*
* Function Name: R_PG_Timer_SetCounterValue_TMR_U0
*
* Description  : Set the TMR counter value
*
* Arguments    : uint16_t counter_val : Value to be set to the counter.
*
* Return Value : true  : Setting of the counter value succeeded.
*              : false : Setting of the counter value failed.
*
* Calling Functions : R_TMR_ControlUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_SetCounterValue_TMR_U0(uint16_t counter_val)
{
	return R_TMR_ControlUnit(
		0,
		PDL_TMR_COUNTER,
		counter_val,
		0,
		0
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_GetRequestFlag_TMR_U0(bool * cma, bool * cmb, bool * ov)
*
* Function Name: R_PG_Timer_GetRequestFlag_TMR_U0
*
* Description  : Acquire and clear the TMR interrupt flags
*
* Arguments    : bool * cma : The address of the storage area for the compare match A flag.
*              : bool * cmb : The address of the storage area for the compare match B flag.
*              : bool * ov : The address of the storage area for the overflow flag.
*
* Return Value : true  : Acquisition of the flags succeeded.
*              : false : Acquisition of the flags failed.
*
* Calling Functions : R_TMR_ReadUnit
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_GetRequestFlag_TMR_U0(bool * cma, bool * cmb, bool * ov)
{
	uint8_t data;
	bool res;

	res = R_TMR_ReadUnit(
		0,
		&data,
		PDL_NO_PTR,
		PDL_NO_PTR,
		PDL_NO_PTR
	);

	if( cma ){
		*cma = (data >> 4) & 0x01;
	}
	if( cmb ){
		*cmb = (data >> 5) & 0x01;
	}
	if( ov ){
		*ov = (data >> 6) & 0x01;
	}

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Timer_StopModule_TMR_U0(void)
*
* Function Name: R_PG_Timer_StopModule_TMR_U0
*
* Description  : Shut down the TMR unit
*
* Arguments    : None
*
* Return Value : true  : Shutting down succeeded.
*              : false : Shutting down failed.
*
* Calling Functions : R_TMR_Destroy
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Timer_StopModule_TMR_U0(void)
{
	return R_TMR_Destroy( 0 );

}



