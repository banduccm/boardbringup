/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_Clock.c
* Version      : 1.00
* Device(s)    : 
* Tool-Chain   : 
* H/W Platform : 
* Description  : 
* Limitations  : 
******************************************************************************
* History : 21.07.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_pdl_cgc.h"
#include "r_pdl_definitions.h"


/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Set(void)
*
* Function Name: R_PG_Clock_Set
*
* Description  : Set up the clocks
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*                   : R_CGC_Set
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Set(void)
{
	bool res;

	res = R_CGC_Set(
		PDL_CGC_CLK_LOCO,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE | PDL_CGC_RTC_SRC_SUB,
		125000,
		125000.000000,
		125000.000000,
		125000.000000,
		125000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_MAIN,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE,
		12000000,
		3000000.000000,
		3000000.000000,
		3000000.000000,
		3000000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_SUB_CLOCK,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE | PDL_CGC_SUB_CLOCK_CL_LOW,
		32768,
		32768.000000,
		32768.000000,
		32768.000000,
		32768.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_SUB_512
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_PLL,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE,
		192000000,
		96000000.000000,
		96000000.000000,
		48000000.000000,
		48000000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		48000000.000000,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	return R_CGC_Control(
		PDL_CGC_CLK_PLL,
		PDL_CGC_OSC_STOP_DISABLE,
		PDL_CGC_RTC_TO_BE_USED
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_WaitSet(double wait_time)
*
* Function Name: R_PG_Clock_WaitSet
*
* Description  : Set up the clocks (including the oscillation stabilization waiting time)
*
* Arguments    : double wait_time : Oscillation stabilization waiting time (in seconds)
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*                   : R_CGC_Set
*                   : rpdl_wait_time
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_WaitSet(double wait_time)
{
	bool res;

	res = R_CGC_Set(
		PDL_CGC_CLK_LOCO,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE | PDL_CGC_RTC_SRC_SUB,
		125000,
		125000.000000,
		125000.000000,
		125000.000000,
		125000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_MAIN,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE,
		12000000,
		3000000.000000,
		3000000.000000,
		3000000.000000,
		3000000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_SUB_CLOCK,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE | PDL_CGC_SUB_CLOCK_CL_LOW,
		32768,
		32768.000000,
		32768.000000,
		32768.000000,
		32768.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_SUB_512
	);

	if( !res ){
		return res;
	}

	res = R_CGC_Set(
		PDL_CGC_CLK_PLL,
		PDL_CGC_BCLK_DISABLE | PDL_CGC_SDCLK_DISABLE,
		192000000,
		96000000.000000,
		96000000.000000,
		48000000.000000,
		48000000.000000,
		PDL_NO_DATA,
		PDL_NO_DATA,
		48000000.000000,
		PDL_NO_DATA
	);

	if( !res ){
		return res;
	}

	rpdl_wait_time( wait_time );

	return R_CGC_Control(
		PDL_CGC_CLK_PLL,
		PDL_CGC_OSC_STOP_DISABLE,
		PDL_CGC_RTC_TO_BE_USED
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Start_LOCO(void)
*
* Function Name: R_PG_Clock_Start_LOCO
*
* Description  : Enable the LOCO
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Start_LOCO(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_LOCO_ENABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Stop_LOCO(void)
*
* Function Name: R_PG_Clock_Stop_LOCO
*
* Description  : Disable the LOCO
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Stop_LOCO(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_LOCO_DISABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Start_MAIN(void)
*
* Function Name: R_PG_Clock_Start_MAIN
*
* Description  : Enable the main clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Start_MAIN(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_MAIN_ENABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Stop_MAIN(void)
*
* Function Name: R_PG_Clock_Stop_MAIN
*
* Description  : Disable the main clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Stop_MAIN(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_MAIN_DISABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Enable_MAIN_ForcedOscillation(void)
*
* Function Name: R_PG_Clock_Enable_MAIN_ForcedOscillation
*
* Description  : Enable forced oscillation of the main clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Enable_MAIN_ForcedOscillation(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_MAIN_FORCED_ENABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Disable_MAIN_ForcedOscillation(void)
*
* Function Name: R_PG_Clock_Disable_MAIN_ForcedOscillation
*
* Description  : Disable forced oscillation of the main clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Disable_MAIN_ForcedOscillation(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_CGC_MAIN_FORCED_DISABLE,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Start_SUB(void)
*
* Function Name: R_PG_Clock_Start_SUB
*
* Description  : Enable the sub-clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Start_SUB(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_SUB_CLOCK_ENABLE
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Stop_SUB(void)
*
* Function Name: R_PG_Clock_Stop_SUB
*
* Description  : Disable the sub-clock oscillator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Stop_SUB(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_SUB_CLOCK_DISABLE
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Start_PLL(void)
*
* Function Name: R_PG_Clock_Start_PLL
*
* Description  : Enable the PLL circuit
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Start_PLL(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_PLL_ENABLE
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_Stop_PLL(void)
*
* Function Name: R_PG_Clock_Stop_PLL
*
* Description  : Disable the PLL circuit
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CGC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_Stop_PLL(void)
{
	return R_CGC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_CGC_PLL_DISABLE
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_GetSelectedClockSource(uint8_t* clock)
*
* Function Name: R_PG_Clock_GetSelectedClockSource
*
* Description  : Acquire the selected clock source
*
* Arguments    : uint8_t* clock : The address of the storage area for the clock source selection.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_CGC_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_GetSelectedClockSource(uint8_t* clock)
{
	uint16_t data;
	bool res;

	res = R_CGC_GetStatus( &data );

	if( clock ){
		*clock = (data >> 4) &0x07;
	}

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_GetClocksStatus(bool* pll, bool* main, bool* sub, bool* loco, bool* iwdt, bool* hoco)
*
* Function Name: R_PG_Clock_GetClocksStatus
*
* Description  : Acquire the status of the clock source
*
* Arguments    : bool* pll : The address of the storage area for the PLL control.
*              : bool* main : The address of the storage area for the main clock control.
*              : bool* sub : The address of the storage area for the sub-clock control.
*              : bool* loco : The address of the storage area for the LOCO control.
*              : bool* iwdt : The address of the storage area for the IWDTLOCO control.
*              : bool* hoco : The address of the storage area for the HOCO control.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_CGC_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_GetClocksStatus(bool* pll, bool* main, bool* sub, bool* loco, bool* iwdt, bool* hoco)
{
	uint16_t data;
	bool res;

	res = R_CGC_GetStatus( &data );

	if( pll ){
		*pll = (data >> 8) &0x01;
	}
	if( main ){
		*main = (data >> 9) &0x01;
	}
	if( sub ){
		*sub = (data >> 10) &0x01;
	}
	if( loco ){
		*loco = (data >> 11) &0x01;
	}
	if( iwdt ){
		*iwdt = (data >> 12) &0x01;
	}
	if( hoco ){
		*hoco = (data >> 13) &0x01;
	}

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_GetHOCOPowerStatus(bool* power)
*
* Function Name: R_PG_Clock_GetHOCOPowerStatus
*
* Description  : Acquire the status of HOCO power
*
* Arguments    : bool* power : The address of the storage area for the HOCO power control.
*
* Return Value : true  : Acquisition succeeded.
*              : false : Acquisition failed.
*
* Calling Functions : R_CGC_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_GetHOCOPowerStatus(bool* power)
{
	uint16_t data;
	bool res;

	res = R_CGC_GetStatus( &data );

	if( power ){
		*power = (data >> 14) &0x01;
	}

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_Clock_GetFlag_MAIN_StopDetection(bool* stop)
*
* Function Name: R_PG_Clock_GetFlag_MAIN_StopDetection
*
* Description  : Acquire the flag of the oscillation stop detection function for the main clock oscillator
*
* Arguments    : bool* stop : The address of the storage area for the oscillation stop detection flag.
*
* Return Value : true  : Acquisition of the flag succeeded.
*              : false : Acquisition of the flag failed.
*
* Calling Functions : R_CGC_GetStatus
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_Clock_GetFlag_MAIN_StopDetection(bool* stop)
{
	uint16_t data;
	bool res;

	res = R_CGC_GetStatus( &data );

	if( stop ){
		*stop = data &0x01;
	}

	return res;
}



