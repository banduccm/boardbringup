/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_CRC.h
* Version      : 1.00
* Description  : 
******************************************************************************
* History : 01.09.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

bool R_PG_CRC_Set(void);
bool R_PG_CRC_InputData(uint8_t data);
bool R_PG_CRC_GetResult(uint16_t * result);
bool R_PG_CRC_StopModule(void);



