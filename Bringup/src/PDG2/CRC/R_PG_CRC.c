/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_CRC.c
* Version      : 1.00
* Device(s)    : 
* Tool-Chain   : 
* H/W Platform : 
* Description  : 
* Limitations  : 
******************************************************************************
* History : 01.09.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_pdl_crc.h"
#include "r_pdl_definitions.h"


/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_CRC_Set(void)
*
* Function Name: R_PG_CRC_Set
*
* Description  : Set up the CRC calculator
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_CRC_Create
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_CRC_Set(void)
{
	return R_CRC_Create( PDL_CRC_POLY_CRC_16 | PDL_CRC_LSB_FIRST );

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_CRC_InputData(uint8_t data)
*
* Function Name: R_PG_CRC_InputData
*
* Description  : Write data to the CRC data input register
*
* Arguments    : uint8_t data : Value to be written.
*
* Return Value : true  : Writing proceeded correctly.
*              : false : Writing failed.
*
* Calling Functions : R_CRC_Write
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_CRC_InputData(uint8_t data)
{
	return R_CRC_Write( data );

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_CRC_GetResult(uint16_t * result)
*
* Function Name: R_PG_CRC_GetResult
*
* Description  : Get the CRC calculation result
*
* Arguments    : uint16_t * result : 
*
* Return Value : true  : Acquisition of the result succeeded.
*              : false : Acquisition of the result failed.
*
* Calling Functions : R_CRC_Read
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_CRC_GetResult(uint16_t * result)
{
	if( result == 0 ){ return false; }

	return R_CRC_Read(
		PDL_CRC_RETAIN_RESULT,
		result
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_CRC_StopModule(void)
*
* Function Name: R_PG_CRC_StopModule
*
* Description  : Shut down the CRC calculation
*
* Arguments    : None
*
* Return Value : true  : Shutting down succeeded.
*              : false : Shutting down failed.
*
* Calling Functions : R_CRC_Destroy
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_CRC_StopModule(void)
{
	return R_CRC_Destroy();

}



