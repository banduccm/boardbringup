/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_RTC.h
* Version      : 1.00
* Description  : 
******************************************************************************
* History : 21.07.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

bool R_PG_RTC_Start(void);
bool R_PG_RTC_WarmStart(void);
bool R_PG_RTC_Stop(void);
bool R_PG_RTC_Restart(void);
bool R_PG_RTC_SetCurrentTime(uint8_t seconds, uint8_t minutes, bool pm, uint8_t hours, uint8_t day, uint8_t month, uint16_t year);
bool R_PG_RTC_GetStatus(bool * hour_mode24, uint8_t * seconds, uint8_t * minutes, bool* pm, uint8_t * hours, uint8_t * day_of_week, uint8_t * day, uint8_t * month, uint16_t * year, bool * carry, bool * alarm, bool * period, bool * adjustment, bool * reset, bool * running);
bool R_PG_RTC_Adjust30sec(void);
bool R_PG_RTC_ManualErrorAdjust(int8_t cycle);
bool R_PG_RTC_Set24HourMode(void);
bool R_PG_RTC_Set12HourMode(void);
bool R_PG_RTC_SetPeriodicInterrupt(float frequency);



