/******************************************************************************
* DISCLAIMER

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized.

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws.

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED.

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software.  
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_RTC.c
* Version      : 1.00
* Device(s)    : 
* Tool-Chain   : 
* H/W Platform : 
* Description  : 
* Limitations  : 
******************************************************************************
* History : 21.07.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_pdl_rtc.h"
#include "r_pdl_definitions.h"
#include "R_PG_IntFuncsExtern.h"


/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Start(void)
*
* Function Name: R_PG_RTC_Start
*
* Description  : Set up and start the RTC
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Create
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Start(void)
{
	return R_RTC_Create(
		PDL_RTC_24_HOUR_MODE | PDL_RTC_OUTPUT_DISABLE,
		PDL_NO_DATA,
		0xFF000000,
		0x00000101,
		PDL_RTC_CAPTURE_EDGE_NONE | PDL_RTC_CAPTURE_FILTER_OFF,
		PDL_RTC_CAPTURE_EDGE_NONE | PDL_RTC_CAPTURE_FILTER_OFF,
		PDL_RTC_CAPTURE_EDGE_NONE | PDL_RTC_CAPTURE_FILTER_OFF,
		PDL_RTC_PERIODIC_1_HZ,
		0xFF000000,
		0x00000101,
		PDL_NO_FUNC,
		0,
		RTCOneSecTimerInterrupt,
		15
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_WarmStart(void)
*
* Function Name: R_PG_RTC_WarmStart
*
* Description  : Re-setup the RTC at warm start up.
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_CreateWarm
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_WarmStart(void)
{
	return R_RTC_CreateWarm(
		PDL_NO_FUNC,
		0,
		RTCOneSecTimerInterrupt,
		15
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Stop(void)
*
* Function Name: R_PG_RTC_Stop
*
* Description  : Stop the clock
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Stop(void)
{
	return R_RTC_Control(
		PDL_RTC_CLOCK_STOP,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Restart(void)
*
* Function Name: R_PG_RTC_Restart
*
* Description  : Restart the clock
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Restart(void)
{
	return R_RTC_Control(
		PDL_RTC_CLOCK_START,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_SetCurrentTime(uint8_t seconds, uint8_t minutes, bool pm, uint8_t hours, uint8_t day, uint8_t month, uint16_t year)
*
* Function Name: R_PG_RTC_SetCurrentTime
*
* Description  : Set the current day and time
*
* Arguments    : uint8_t seconds : Second (value range: 0x00 to 0x59 (in BCD format))
*              : uint8_t minutes : Minute (value range: 0x00 to 0x59 (in BCD format))
*              : bool pm : p.m.
*              :         : ( 0�Fa.m. or 24-hour mode 1�Fp.m.)
*              : uint8_t hours : Hour (value range: 0x00 to 0x23 (in BCD format))
*              : uint8_t day : Date (value range: 0x01 to the number of days in the specified month (in BCD format))
*              : uint8_t month : Month (value range: 0x01 to 0x12 (in BCD format))
*              : uint16_t year : Year (value range: 0x0000 to 0x9999 (in BCD format))
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_SetCurrentTime(uint8_t seconds, uint8_t minutes, bool pm, uint8_t hours, uint8_t day, uint8_t month, uint16_t year)
{
	uint32_t tmp = 0;
	uint32_t data_time = 0xFF000000;
	uint32_t data_date = 0;

	tmp = pm;
	tmp = hours | (tmp << 7);
	data_time |= (tmp << 16);
	
	tmp = minutes;
	data_time |= (tmp << 8);
	
	data_time |= seconds;
	
	tmp = year;
	data_date |= (tmp << 16);
	
	tmp = month;
	data_date |= (tmp << 8);
	
	data_date |= day;
	

	return R_RTC_Control(
		PDL_NO_DATA,
		PDL_RTC_UPDATE_CURRENT_TIME | PDL_RTC_UPDATE_CURRENT_DATE,
		data_time,
		data_date,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_GetStatus(bool * hour_mode24, uint8_t * seconds, uint8_t * minutes, bool* pm, uint8_t * hours, uint8_t * day_of_week, uint8_t * day, uint8_t * month, uint16_t * year, bool * carry, bool * alarm, bool * period, bool * adjustment, bool * reset, bool * running)
*
* Function Name: R_PG_RTC_GetStatus
*
* Description  : Get the current time and status
*
* Arguments    : bool * hour_mode24 : The address of the storage area for the hours mode
*              :                    : (0: 24-hour mode 1: 12-hour mode)
*              : uint8_t * seconds : The address of the storage area for the current seconds
*              : uint8_t * minutes : The address of the storage area for the current minutes
*              : bool* pm : The address of the storage area for p.m
*              : uint8_t * hours : The address of the storage area for the current hours
*              : uint8_t * day_of_week : The address of the storage area for the current day of the week
*              : uint8_t * day : The address of the storage area for the current day
*              : uint8_t * month : The address of the storage area for the current month
*              : uint16_t * year : The address of the storage area for the current year
*              : bool * carry : The address of the storage area for the carry interrupt flag
*              : bool * alarm : The address of the storage area for the alarm interrupt flag
*              : bool * period : The address of the storage area for the periodic interrupt flag
*              : bool * adjustment : The address of the storage area for the 30-second adjustment bit
*              :                   : ( 0:Normal operation  1:Adjustment in progress )
*              : bool * reset : The address of the storage area for the reset bit
*              :              : ( 0:Normal operation  1:Reset in progress )
*              : bool * running : The address of the storage area for the start bit
*              :                : ( 0:Clock is stopped  1:Clock is running )
*
* Return Value : true  : Acquisition of the flags succeeded.
*              : false : Acquisition of the flags failed.
*
* Calling Functions : R_RTC_Read
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_GetStatus(bool * hour_mode24, uint8_t * seconds, uint8_t * minutes, bool* pm, uint8_t * hours, uint8_t * day_of_week, uint8_t * day, uint8_t * month, uint16_t * year, bool * carry, bool * alarm, bool * period, bool * adjustment, bool * reset, bool * running)
{
	uint8_t status;
	uint32_t data_time;
	uint32_t data_date;
	bool tmp_mode24;
	bool res;

	res = R_RTC_Read(
		PDL_RTC_READ_CURRENT,
		&status,
		&data_time,
		&data_date
	);

	if( running ){
		*running = status & 0x01;
	}
	if( reset ){
		*reset = (status >> 1) & 0x01;
	}
	if( adjustment ){
		*adjustment = (status >> 2) & 0x01;
	}
	if( alarm ){
		*alarm = (status >> 4) & 0x01;
	}
	if( period ){
		*period = (status >> 5) & 0x01;
	}
	if( carry ){
		*carry = (status >> 6) & 0x01;
	}
	tmp_mode24 = (status >> 7) & 0x01;
	if( hour_mode24 ){
		*hour_mode24 = tmp_mode24;
	}
	
	if( seconds ){
		*seconds = data_time & 0x000000FF;
	}
	if( minutes ){
		*minutes = (data_time >> 8) & 0x000000FF;
	}
	if( hours ){
		*hours = (data_time >> 16) & 0x000000FF;
		if ( tmp_mode24 == 0 ) {
			if ( pm ) {
				*pm =  ( *hours >> 7 ) & 0x00000001;
			}
			*hours &= 0x0000007F;
		}
	}
	if( day_of_week ){
		*day_of_week = (data_time >> 24) & 0x000000FF;
	}
	
	if( day ){
		*day = data_date & 0x000000FF;
	}
	if( month ){
		*month = (data_date >> 8) & 0x000000FF;
	}
	if( year ){
		*year = (data_date >> 16) & 0x0000FFFF;
	}
	

	return res;
}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Adjust30sec(void)
*
* Function Name: R_PG_RTC_Adjust30sec
*
* Description  : Perform 30-second adjustment
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Adjust30sec(void)
{
	return R_RTC_Control(
		PDL_RTC_ADJUST_START,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_ManualErrorAdjust(int8_t cycle)
*
* Function Name: R_PG_RTC_ManualErrorAdjust
*
* Description  : Adjust the time error
*
* Arguments    : int8_t cycle : Error adjustment value (value range: -63�`63)
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_ManualErrorAdjust(int8_t cycle)
{
	uint16_t fos;
	uint8_t adj;

	if ( (cycle < -63) || (cycle > 63) )
	{
		return false;
	}
	if (cycle < 0)
	{
		fos = PDL_RTC_ERROR_ADJUST_MINUS;
		adj = (uint8_t)(cycle * -1);
	}
	else
	{
		fos = PDL_RTC_ERROR_ADJUST_PLUS;
		adj = (uint8_t)cycle;
	}
	

	return R_RTC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_RTC_ERROR_AUTO_ADJUST_DISABLE  | fos | PDL_RTC_ERROR_UPDATE_ERROR_ADJUST_VALUE | adj,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Set24HourMode(void)
*
* Function Name: R_PG_RTC_Set24HourMode
*
* Description  : Set the RTC to 24-hour mode
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Set24HourMode(void)
{
	return R_RTC_Control(
		PDL_RTC_24_HOUR_MODE,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_Set12HourMode(void)
*
* Function Name: R_PG_RTC_Set12HourMode
*
* Description  : Set the RTC to 12-hour mode
*
* Arguments    : None
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_Set12HourMode(void)
{
	return R_RTC_Control(
		PDL_RTC_12_HOUR_MODE,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA
	);

}

/******************************************************************************
* ID           : 
*
* Include      : 
*
* Declaration  : bool R_PG_RTC_SetPeriodicInterrupt(float frequency)
*
* Function Name: R_PG_RTC_SetPeriodicInterrupt
*
* Description  : Configure the periodic interrupt
*
* Arguments    : float frequency : Frequency used for interrupt generation (Hz) (valid values: 0.5, 1, 2, 4, 8, 16, 32, 64, 128, 256)
*
* Return Value : true  : Setting was made correctly.
*              : false : Setting failed.
*
* Calling Functions : R_RTC_Control
*
* Details      : Please refer to the Reference Manual.
******************************************************************************/
bool R_PG_RTC_SetPeriodicInterrupt(float frequency)
{
	uint16_t data;

	if( frequency == 256 ){
		data = PDL_RTC_PERIODIC_256_HZ;
	}
	else if( frequency == 128 ){
		data = PDL_RTC_PERIODIC_128_HZ;
	}
	else if( frequency == 64 ){
		data = PDL_RTC_PERIODIC_64_HZ;
	}
	else if( frequency == 32 ){
		data = PDL_RTC_PERIODIC_32_HZ;
	}
	else if( frequency == 16 ){
		data = PDL_RTC_PERIODIC_16_HZ;
	}
	else if( frequency == 8 ){
		data = PDL_RTC_PERIODIC_8_HZ;
	}
	else if( frequency == 4 ){
		data = PDL_RTC_PERIODIC_4_HZ;
	}
	else if( frequency == 2 ){
		data = PDL_RTC_PERIODIC_2_HZ;
	}
	else if( frequency == 1 ){
		data = PDL_RTC_PERIODIC_1_HZ;
	}
	else if( frequency == 0.5 ){
		data = PDL_RTC_PERIODIC_2S;
	}
	else{
		return false;
	}
	

	return R_RTC_Control(
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		PDL_NO_DATA,
		data
	);

}



