/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_IntFuncsExtern.h
* Version      : 
* Description  : 
******************************************************************************
* History : 09.09.2015 Version Description
*         :   
******************************************************************************/



extern void ControlTaskTimerInterrupt(void);
extern void RTCOneSecTimerInterrupt(void);
extern void Sci7TrFunc(void);
extern void S12ad0IntFunc(void);



