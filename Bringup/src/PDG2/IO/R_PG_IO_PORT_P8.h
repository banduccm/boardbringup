/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_IO_PORT_P8.h
* Version      : 1.00
* Description  : 
******************************************************************************
* History : 12.05.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

bool R_PG_IO_PORT_Set_P8(void);
bool R_PG_IO_PORT_Set_P83(void);
bool R_PG_IO_PORT_Set_P86(void);
bool R_PG_IO_PORT_Set_P87(void);
bool R_PG_IO_PORT_Read_P8(uint8_t * data);
bool R_PG_IO_PORT_Read_P80(uint8_t * data);
bool R_PG_IO_PORT_Read_P81(uint8_t * data);
bool R_PG_IO_PORT_Read_P82(uint8_t * data);
bool R_PG_IO_PORT_Read_P83(uint8_t * data);
bool R_PG_IO_PORT_Read_P86(uint8_t * data);
bool R_PG_IO_PORT_Read_P87(uint8_t * data);
bool R_PG_IO_PORT_Write_P8(uint8_t data);
bool R_PG_IO_PORT_Write_P83(uint8_t data);
bool R_PG_IO_PORT_Write_P86(uint8_t data);
bool R_PG_IO_PORT_Write_P87(uint8_t data);



