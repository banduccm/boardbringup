/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_IO_PORT_P6.h
* Version      : 1.00
* Description  : 
******************************************************************************
* History : 12.05.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

bool R_PG_IO_PORT_Set_P6(void);
bool R_PG_IO_PORT_Set_P60(void);
bool R_PG_IO_PORT_Set_P61(void);
bool R_PG_IO_PORT_Set_P62(void);
bool R_PG_IO_PORT_Set_P63(void);
bool R_PG_IO_PORT_Set_P64(void);
bool R_PG_IO_PORT_Set_P67(void);
bool R_PG_IO_PORT_Read_P6(uint8_t * data);
bool R_PG_IO_PORT_Read_P60(uint8_t * data);
bool R_PG_IO_PORT_Read_P61(uint8_t * data);
bool R_PG_IO_PORT_Read_P62(uint8_t * data);
bool R_PG_IO_PORT_Read_P63(uint8_t * data);
bool R_PG_IO_PORT_Read_P64(uint8_t * data);
bool R_PG_IO_PORT_Read_P65(uint8_t * data);
bool R_PG_IO_PORT_Read_P66(uint8_t * data);
bool R_PG_IO_PORT_Read_P67(uint8_t * data);
bool R_PG_IO_PORT_Write_P6(uint8_t data);
bool R_PG_IO_PORT_Write_P60(uint8_t data);
bool R_PG_IO_PORT_Write_P61(uint8_t data);
bool R_PG_IO_PORT_Write_P62(uint8_t data);
bool R_PG_IO_PORT_Write_P63(uint8_t data);
bool R_PG_IO_PORT_Write_P64(uint8_t data);
bool R_PG_IO_PORT_Write_P67(uint8_t data);



