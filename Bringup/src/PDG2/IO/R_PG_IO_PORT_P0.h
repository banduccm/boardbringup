/******************************************************************************
* DISCLAIMER
* Please refer to http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010-2013 Renesas Electronics Corporation.
* and Renesas Solutions Corporation. All rights reserved.
******************************************************************************
* File Name    : R_PG_IO_PORT_P0.h
* Version      : 1.00
* Description  : 
******************************************************************************
* History : 12.05.2015 Version Description
*         :   
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include <stdbool.h>

bool R_PG_IO_PORT_Set_P0(void);
bool R_PG_IO_PORT_Set_P01(void);
bool R_PG_IO_PORT_Set_P02(void);
bool R_PG_IO_PORT_Set_P03(void);
bool R_PG_IO_PORT_Set_P05(void);
bool R_PG_IO_PORT_Set_P07(void);
bool R_PG_IO_PORT_Read_P0(uint8_t * data);
bool R_PG_IO_PORT_Read_P00(uint8_t * data);
bool R_PG_IO_PORT_Read_P01(uint8_t * data);
bool R_PG_IO_PORT_Read_P02(uint8_t * data);
bool R_PG_IO_PORT_Read_P03(uint8_t * data);
bool R_PG_IO_PORT_Read_P05(uint8_t * data);
bool R_PG_IO_PORT_Read_P07(uint8_t * data);
bool R_PG_IO_PORT_Write_P0(uint8_t data);
bool R_PG_IO_PORT_Write_P01(uint8_t data);
bool R_PG_IO_PORT_Write_P02(uint8_t data);
bool R_PG_IO_PORT_Write_P03(uint8_t data);
bool R_PG_IO_PORT_Write_P05(uint8_t data);
bool R_PG_IO_PORT_Write_P07(uint8_t data);



