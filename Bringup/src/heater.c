/*
 * heater.c
 *
 *  Created on: May 27, 2015
 *      Author: Mike
 */

#include "heater.h"
#include "testIntercept.h"

/************************************************************
 * bool heaterInit(void)                                    *
 * Initializes all peripherals used by the heater.          *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool heaterInit(void)
{
    bool success = false;

    success = gpioInit();

    if(success)
    {
        success = adcInit();
    }

    if(success)
    {
        success = adcStart();
    }

    return success;
}

/************************************************************
 * bool heaterEnable(heater_t* pHeater, bool enable)        *
 * Sets the given heater's enable pin to the given state.   *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool heaterControl(heater_t* pHeater, bool enable)
{
    bool success = false;
    gpioState_t enablePin = (enable)?eGpioStateInactive:eGpioStateActive;

    if(pHeater != NULL)
    {
        success = gpioWrite(&(pHeater->enable), enablePin);

        if(success)
        {
            pHeater->enableStatus = enable;
        }
    }

    return success;
}

/************************************************************
 * bool heaterOvercurrent(heater_t* pHeater, bool* pOC)     *
 * Reads the heater's overcurrent pin.                      *
 *                                                          *
 * Returns:                                                 *
 *  TRUE if all operations succeed; FALSE otherwise         *
 ***********************************************************/
bool heaterOvercurrent(heater_t* pHeater, bool* pOC)
{
    bool success = false;
    gpioState_t state = eGpioStateInvalid;

    if((pHeater != NULL) && (pOC != NULL))
    {
        success = gpioRead(&(pHeater->overcurrent), &state);
    }

    if(success)
    {
        *pOC = (state == eGpioStateActive)?true:false;
    }

    return success;
}

bool heaterGetCurrent(heater_t* pHeater, uint16_t* current_mA)
{
    bool success = false;
    //@TODO: Make ADC work.
    return success;
}

bool heaterPairControl(heaterPair_t* pHeaterPair, bool enable)
{
    bool success = true;

    if(NULL != pHeaterPair)
    {
        success &= heaterControl(&(pHeaterPair->mainHeater), enable);
        success &= heaterControl(&(pHeaterPair->wrapHeater), enable);
        pHeaterPair->activeEnable = enable;
    }
    else
    {
        success = false;
    }

    return success;
}

bool heaterPairRequest(heaterPair_t* pHeaterPair, bool enable)
{
    bool success = true;

    if(NULL != pHeaterPair)
    {
        pHeaterPair->activeRequest = enable;
    }
    else
    {
        success = false;
    }

    return success;
}
