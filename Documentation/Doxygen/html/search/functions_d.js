var searchData=
[
  ['s12ad0intfunc',['S12ad0IntFunc',['../adc_8c.html#ac07507aabaa9993c858fae10ef4ac351',1,'S12ad0IntFunc(void):&#160;adc.c'],['../_r___p_g___int_funcs_extern_8h.html#ac07507aabaa9993c858fae10ef4ac351',1,'S12ad0IntFunc(void):&#160;adc.c']]],
  ['sbrk',['sbrk',['../sbrk_8c.html#afb395900bf6e112781e9727c9d19c8f6',1,'sbrk.c']]],
  ['sci7trfunc',['Sci7TrFunc',['../_bringup_8c.html#a7800198ad09916c60d5534511d832dad',1,'Sci7TrFunc(void):&#160;Bringup.c'],['../_r___p_g___int_funcs_extern_8h.html#a7800198ad09916c60d5534511d832dad',1,'Sci7TrFunc(void):&#160;Bringup.c']]],
  ['searchbox',['SearchBox',['../search_8js.html#a52066106482f8136aa9e0ec859e8188f',1,'search.js']]],
  ['searchresults',['SearchResults',['../search_8js.html#a9189b9f7a32b6bc78240f40348f7fe03',1,'search.js']]],
  ['setclassattr',['setClassAttr',['../search_8js.html#a499422fc054a5278ae32801ec0082c56',1,'search.js']]],
  ['setkeyactions',['setKeyActions',['../search_8js.html#a98192fa2929bb8e4b0a890a4909ab9b2',1,'search.js']]],
  ['spi1intfunc',['Spi1IntFunc',['../spi_8c.html#aaa52919ea59374a64af8fd4c1a9b95a1',1,'spi.c']]],
  ['spiflashinit',['spiFlashInit',['../spi_flash_8h.html#a78ff4b0843b6bcc508203ce18464e39b',1,'spiFlashInit(void):&#160;spiFlash.c'],['../spi_flash_8c.html#a78ff4b0843b6bcc508203ce18464e39b',1,'spiFlashInit(void):&#160;spiFlash.c']]],
  ['spiflashread',['spiFlashRead',['../spi_flash_8h.html#ab7fafd2cf078bfd8b89d61877f23fcea',1,'spiFlashRead(uint32_t address, uint8_t *pData, uint32_t bytes):&#160;spiFlash.c'],['../spi_flash_8c.html#ab7fafd2cf078bfd8b89d61877f23fcea',1,'spiFlashRead(uint32_t address, uint8_t *pData, uint32_t bytes):&#160;spiFlash.c']]],
  ['spiflashwrite',['spiFlashWrite',['../spi_flash_8h.html#a3898c7baf5177e5b615368706857730d',1,'spiFlashWrite(uint32_t address, uint8_t *pData, uint32_t bytes):&#160;spiFlash.c'],['../spi_flash_8c.html#a3898c7baf5177e5b615368706857730d',1,'spiFlashWrite(uint32_t address, uint8_t *pData, uint32_t bytes):&#160;spiFlash.c']]],
  ['spiflashwriteenable',['spiFlashWriteEnable',['../spi_flash_8c.html#a5800c4e62b577fe5a25800790d898074',1,'spiFlash.c']]],
  ['spiinit',['spiInit',['../spi_8h.html#a9e88417f5c5c836e4995f253de1c4485',1,'spiInit(void):&#160;spi.c'],['../spi_8c.html#a9e88417f5c5c836e4995f253de1c4485',1,'spiInit(void):&#160;spi.c']]],
  ['spitransfer',['spiTransfer',['../spi_8h.html#ab25c0c01ec7009d9227df7f03d0a0bf0',1,'spiTransfer(spiDevice_t device, uint32_t *txData, uint32_t *rxData, uint32_t count):&#160;spi.c'],['../spi_8c.html#ab25c0c01ec7009d9227df7f03d0a0bf0',1,'spiTransfer(spiDevice_t device, uint32_t *txData, uint32_t *rxData, uint32_t count):&#160;spi.c']]]
];
