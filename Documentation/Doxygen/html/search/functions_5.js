var searchData=
[
  ['getxpos',['getXPos',['../search_8js.html#a76d24aea0009f892f8ccc31d941c0a2b',1,'search.js']]],
  ['getypos',['getYPos',['../search_8js.html#a8d7b405228661d7b6216b6925d2b8a69',1,'search.js']]],
  ['gpioinit',['gpioInit',['../gpio_8h.html#ad5c5c7700e5e6487d0933a4d0cc30d1d',1,'gpioInit(void):&#160;gpio.c'],['../gpio_8c.html#ad5c5c7700e5e6487d0933a4d0cc30d1d',1,'gpioInit(void):&#160;gpio.c']]],
  ['gpioread',['gpioRead',['../gpio_8h.html#a3dc71fcbde9390d5334076dd202bab10',1,'gpioRead(gpio_t *, gpioState_t *):&#160;gpio.c'],['../gpio_8c.html#a3df5a8dc2bafae5f825d58f11a1e2685',1,'gpioRead(gpio_t *pGpio, gpioState_t *pState):&#160;gpio.c']]],
  ['gpiowrite',['gpioWrite',['../gpio_8h.html#a95b675d7470eb1ad0813112336bcf6b3',1,'gpioWrite(gpio_t *, gpioState_t):&#160;gpio.c'],['../gpio_8c.html#a79a70ce9180f1aa39ae011a1b1a623e4',1,'gpioWrite(gpio_t *pGpio, gpioState_t state):&#160;gpio.c']]]
];
